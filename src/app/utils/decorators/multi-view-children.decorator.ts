import { ViewChildren } from '@angular/core';

export function MultiViewChildren(selector, opts: { read: any[] }) {
   const tokens = opts.read;
   const decs = tokens.map(x => ViewChildren(selector, { read: x }));

  return function(target: any, name: string) {
    decs.forEach((d, i) => d(target, `${name}_${i}`));
    Object.defineProperty(target, name, {
        get: function() {
          return decs.map((x, i) =>  this[`${name}_${i}`], this);
        }
      });
  };
}
