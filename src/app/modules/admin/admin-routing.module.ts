import { NgModule } from '@angular/core';

import { ZonePageComponent } from './components/zone-page/zone-page.component';
import { StationPageComponent } from './components/station-page/station-page.component';
import { LinePageComponent } from './components/line-page/line-page.component';
import { SchedulePageComponent } from './components/schedule-page/schedule-page.component';

import { TicketRequestsPageComponent } from './components/ticket-requests-page/ticket-requests-page.component';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AdminGuard } from './guards/admin.guard';
import { TicketPricePageComponent } from './components/ticket-price-page/ticket-price-page.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [AdminGuard],
    children: [
      { path: 'zones', component: ZonePageComponent },
      { path: 'stations', component: StationPageComponent },
      { path: 'lines', component: LinePageComponent },
      { path: 'ticket-requests', component: TicketRequestsPageComponent },
      { path: 'schedules', component: SchedulePageComponent },
      { path: 'ticket-prices', component: TicketPricePageComponent }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule,
  ]
})
export class AdminRoutingModule {
}
