import { Component, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { ZoneService } from 'src/app/zones/zone.service';
import { MatDatepickerInputEvent, MatTableDataSource, MatPaginator, MatSort, MatSnackBar, MatDialog } from '@angular/material';
import { TicketPriceService } from 'src/app/tickets/services/ticket-price.service';
import { SelectionModel } from '@angular/cdk/collections';
import { TicketPrice } from 'src/app/tickets/models/ticket-price.model';

@Component({
  selector: 'ticket-price-page-component',
  templateUrl: './ticket-price-page.component.html',
  styleUrls: [ './ticket-price-page.component.css' ]
})
export class TicketPricePageComponent {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor (
    private zoneService: ZoneService,
    private ticketPriceService: TicketPriceService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
    this.ticketPrice = new TicketPrice();
  }

  ngOnInit() {
    this.getZones();
    this.loadData();
    this.minStartDate = new Date();
    this.minEndDate = new Date();
  }

  STATUSES = [
    {
      name: 'Student',
      value: 'STUDENT'
    },
    {
      name: 'Adult',
      value: 'ADULT'
    },
    {
      name: 'Senior',
      value: 'SENIOR'
    }
  ];

  DURATIONS = [
    {
      name: 'Single use',
      value: 'SINGLE_USE'
    },
    {
      name: 'Day',
      value: 'DAY'
    },
    {
      name: 'Month',
      value: 'MONTH'
    },
    {
      name: 'Year',
      value: 'YEAR'
    }
  ]

  displayedColumns: string[] = [
    'customerStatus',
    'ticketDuration',
    'validFrom',
    'validTo',
    'zone',
    'price',
    'select'
  ];

  zones = [];
  minStartDate;
  minEndDate;
  customerStatus = new FormControl('', [Validators.required]);
  ticketDuration = new FormControl('', [Validators.required]);
  ticketZone = new FormControl('', [Validators.required]);
  price = new FormControl('', [Validators.required]);
  startDate = new FormControl(null, [Validators.required]);
  endDate = new FormControl(null, [Validators.required]);
  dataSource;
  private selection = new SelectionModel<Object>(true, []);
  private ticketPrice: TicketPrice;
  id;
  notUpdatableId = {
    customerStatus: '',
    ticketDuration: '',
    ticketZone: ''
  };


  getZones() {
    this.zoneService.getZones().subscribe(result => this.zones = result);
  }

  loadData() {
    this.ticketPriceService.loadData()
      .subscribe(result => {
        this.dataSource = new MatTableDataSource<Object>(result);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  onStartDateInput(event: MatDatepickerInputEvent<Date>) {
    const date: string[] = event.value.toLocaleDateString().split('/');
    // this.startDate = `${date[1]}.${date[0]}.${date[2]}.`;
    this.startDate = new FormControl(date);
    this.minEndDate = event.value;
  }

  onEndDateInput(event: MatDatepickerInputEvent<Date>) {
    const date: string[] = event.value.toLocaleDateString().split('/');
    // this.endDate = `${date[1]}.${date[0]}.${date[2]}.`;
    this.endDate = new FormControl(date);
  }

  save() {
    if (this.ticketPrice.customerId === null) {
      this.ticketPriceService.create(this.customerStatus.value, this.ticketDuration.value, this.price.value, this.ticketZone.value, this.startDate.value, this.endDate.value)
      .subscribe(
        result => {
          this.loadData();
          this.snackBar.open("Successfully created", "", {
            duration: 4000,
          });
        },
        err => {
          this.snackBar.open("Error", "", {
            duration: 4000,
          });
        }
      );
    } else {
      this.ticketPriceService.update(this.notUpdatableId.customerStatus, this.notUpdatableId.ticketDuration, this.price.value, this.notUpdatableId.ticketZone, this.startDate.value, this.endDate.value)
        .subscribe(
          result => {
            this.loadData();
            this.snackBar.open("Successfully updated", "", {
              duration: 4000,
            });
          },
          err => {
            this.snackBar.open("Error", "", {
              duration: 4000,
            });
          }
        )
    }

    this.clear();
  }

  private applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private select(element) {
    this.ticketPrice = Object.assign(new Object(), element);
    this.customerStatus.setValue(element.id.customerStatus);
    this.ticketDuration.setValue(element.id.ticketDuration);
    this.ticketZone.setValue(element.id.zone.id);
    this.notUpdatableId.customerStatus = this.customerStatus.value;
    this.notUpdatableId.ticketDuration= this.ticketDuration.value;
    this.notUpdatableId.ticketZone = this.ticketZone.value;
    this.price.setValue(element.value);
    this.startDate.setValue(element.validityPeriod.startDate);
    this.endDate.setValue(element.validityPeriod.endDate);
  }

  private clear(): void {
    this.ticketPrice = new TicketPrice();
    this.customerStatus.reset();
    this.ticketDuration.reset();
    this.ticketZone.reset();
    this.price.reset();
    this.startDate.reset();
    this.endDate.reset();
  }

  deleteTicketPrices() {
    this.selection.selected.forEach(el => {
      this.ticketPriceService.delete(el['id'].zone.id, el['id'].ticketDuration, el['id'].customerStatus).subscribe(
        element => {
          const index = this.dataSource.data.findIndex(e =>
            e.id.zone.id == element['id'].zone.id && e.id.ticketDuration === element['id'].ticketDuration && e.id.customerStatus === element['id'].customerStatus
          );
          const data = this.dataSource.data;
          data.splice(index, 1);
          this.dataSource.data = data;
        },
        err => {
          console.log(err);
        }
      );
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(ConfirmDeleteDialog);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.deleteTicketPrices();
      }
    });
  }

}

@Component({
  selector: 'confirm-delete-dialog',
  templateUrl: 'confirm-delete-dialog.html',
})
export class ConfirmDeleteDialog {}
