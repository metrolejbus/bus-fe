import { TicketPricePageComponent } from "./ticket-price-page.component";
import { TestBed, ComponentFixture, async } from "@angular/core/testing";
import { AppMaterialModule } from "./../../../../app-material/app-material.module";
import { DebugElement } from "@angular/core";
import { By } from "@angular/platform-browser";
import { FormsModule, FormControl, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { ZoneService } from "src/app/common/services/zone.service";

describe('TicketPricePageComponent', () => {
  let component: TicketPricePageComponent;
  let fixture: ComponentFixture<TicketPricePageComponent>;
  let zoneService: any;

  beforeEach(() => {
    let zoneServiceMock = {
      getStudents: jasmine.createSpy('getZones')
          .and.returnValue(Promise.resolve([{}, {}, {}])),
      RegenerateData$: {
        subscribe: jasmine.createSpy('subscribe')
      }
    };

    TestBed.configureTestingModule({
      declarations: [ TicketPricePageComponent ],
      imports: [
        AppMaterialModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
      ],
      providers: [
        { provide: ZoneService, useValue: zoneServiceMock }
      ]
    });

    fixture = TestBed.createComponent(TicketPricePageComponent);
    component = fixture.componentInstance;
    zoneService = TestBed.get(ZoneService);
  });

  it('should fetch the list of zones on init', async(() => {
    component.ngOnInit();

    expect(zoneService.RegenerateData$.subscribe).toHaveBeenCalled();
    expect(zoneService.getZones).toHaveBeenCalled();

    fixture.whenStable()
      .then(() => {
        expect(component.zones.length).toBe(3);
        fixture.detectChanges();
        let elements: DebugElement[] = fixture.debugElement.queryAll(By.css('table tr'));
        expect(elements.length).toBe(4);
      });
  }));


});