import {
  Component,
  ViewChild,
  OnInit,
} from '@angular/core';
import {
  FormControl,
  Validators
} from '@angular/forms';
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatDialog
} from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import {
  latLng,
  tileLayer
} from 'leaflet';

import { Station } from 'src/app/common/models/station.model';
import { Zone } from 'src/app/common/models/zone.model';
import { ZoneService } from 'src/app/common/services/zone.service';
import { StationService } from 'src/app/common/services/station.service';
import { StationMapDialogComponent } from './station-map-dialog.component';


@Component({
  selector: 'app-station-page',
  templateUrl: './station-page.component.html',
  styleUrls: [ './station-page.component.css' ],
})
export class StationPageComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  private displayedColumns: string[] = ['select', 'id', 'name', 'zone', 'lat', 'lng'];
  private dataSource: MatTableDataSource<Station>;
  private selection = new SelectionModel<Station>(true, []);

  private name: FormControl;
  private zone: FormControl;
  private lat: FormControl;
  private lng: FormControl;

  private station: Station;
  private zones: Zone[];

  public constructor(
    private dialog: MatDialog,
    private zoneService: ZoneService,
    private stationService: StationService,
  ) {

  }

  ngOnInit() {
    this.station = new Station();

    this.name = new FormControl('', [Validators.required]);
    this.zone = new FormControl('', [Validators.required, (c: FormControl) => this.validateZone(c)]);
    this.lat = new FormControl(
      '', [Validators.required, Validators.min(-90), Validators.max(90)]
    );
    this.lng = new FormControl(
      '', [Validators.required, Validators.min(-180), Validators.max(180)]
    );

    this.dataSource = new MatTableDataSource<Station>([]);
    this.dataSource.filterPredicate = (station, name) => station.name.toLowerCase().includes(name.toLowerCase());
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.stationService.getStations().subscribe(
      data => {
        this.dataSource = new MatTableDataSource<Station>(data);
        this.dataSource.filterPredicate = (station, name) => {
          return station.name.toLowerCase().includes(name.toLowerCase());
        };

        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );

    this.zones = [];
    this.zoneService.getZones().subscribe(
      data => {
        this.zones = data;
      }
    );
  }

  private save(): void {
    const name = this.name.value;
    const lat = this.lat.value;
    const lng = this.lng.value;
    const zone = this.zone.value;

    if (this.station.id == null) {
      this.stationService.createStation(name, zone.id, lat, lng).subscribe(
        station => {
          const data = this.dataSource.data;
          data.push(station);
          this.dataSource.data = data;
        }
      );
    } else {
      this.stationService.updateStation(this.station.id, name, zone.id, lat, lng).subscribe(
        station => {
          const data = this.dataSource.data;
          const index = data.findIndex(s => s.id === station.id);
          data.splice(index, 1, station);
          this.dataSource.data = data;
        }
      );
    }

    this.clear();
  }

  private clear(): void {
    this.station = new Station();
    this.name.reset();
    this.zone.reset();
    this.lat.reset();
    this.lng.reset();
  }

  private select(element: Station, e: MouseEvent) {
    this.station = Object.assign(new Station(), element);
    this.name.setValue(element.name);
    this.zone.setValue(element.zone);
    this.lat.setValue(element.location.lat);
    this.lng.setValue(element.location.lng);
  }

  private delete(): void {
    this.selection.selected.forEach(el => {
      this.stationService.deleteStation(el.id).subscribe(
        station => {
          const index = this.dataSource.data
            .findIndex(s => s.id === station.id);
          const data = this.dataSource.data;
          data.splice(index, 1);
          this.dataSource.data = data;
        }
      );
    });

    this.selection.clear();
  }

  private openMapDialog(): void {
    const dialogRef = this.dialog.open(StationMapDialogComponent, {
      width: '700px',
      height: '700px',
      data: {
        lat: this.station.location.lat,
        lng: this.station.location.lng,
        map: {
          zoom: 14,
          center: latLng(51.5076, -0.1177),
          layers: [
            tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
              minZoom: 11,
              maxZoom: 18,
              attribution: '...'
            }),
          ],
        },
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const { lat, lng } = result;

        this.station.location.lat = lat;
        this.station.location.lng = lng;

        this.lat.setValue(lat);
        this.lng.setValue(lng);
      }
    });
  }

  private applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private areFieldsValid(): boolean {
    return this.name.value && this.zone.valid && this.lat.valid && this.lat.valid;
  }

  private getZoneName(zone?: Zone): string {
    return zone ? zone.name : undefined;
  }

  private validateZone(c: FormControl): object {
    if (!c.value) {
      return null;
    }
    const zone = this.zones.find(z => z.name === c.value.name);
    return !zone ? { notfound: 'Zone with entered name not found, please select zone from the list' } : null;
  }
}
