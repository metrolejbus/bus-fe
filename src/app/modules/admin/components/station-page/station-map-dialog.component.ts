import {
  Component,
  Inject,
} from '@angular/core';
import {
  MatDialogRef,
  MatSnackBar,
  MAT_DIALOG_DATA,
} from '@angular/material';
import {
  FormControl,
  Validators,
} from '@angular/forms';

import * as L from 'leaflet';

@Component({
  selector: 'app-station-map-dialog',
  templateUrl: './station-map-dialog.component.html',
  styleUrls: [ './station-map-dialog.component.css' ]
})
export class StationMapDialogComponent {

  private lat: FormControl;
  private lng: FormControl;

  private stationIcon: L.DivIcon;

  private leafletOptions: L.MapOptions;

  private leafletLayers: L.Layer[];

  constructor(
    public dialogRef: MatDialogRef<StationMapDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      lat: number,
      lng: number,
      map: L.MapOptions
    },
    public snackBar: MatSnackBar
  ) {
    this.lat = new FormControl(data.lat || '', [Validators.required]);
    this.lng = new FormControl(data.lng || '', [Validators.required]);

    this.leafletOptions = data.map;
    this.leafletLayers = [];

    this.stationIcon = L.divIcon({
      html: `
        <i
          class="material-icons material-icons.md-18"
          style="position: relative; left: -5px; top: -15px; color: blue;"
        >place</i>
      `,
      iconSize: [18, 18],
    });

    if (data.lat && data.lng) {
      this.leafletLayers.push(
        L.marker([data.lat, data.lng], { icon: this.stationIcon })
      );
      this.leafletOptions.center = L.latLng(data.lat, data.lng);
    }
  }

  private dismiss(): void {
    this.dialogRef.close();
  }

  private apply(): void {
    const lat = this.lat.value;
    const lng = this.lng.value;

    this.dialogRef.close({ lat, lng });
  }

  private onMapClick(event: any) {
    this.leafletLayers.pop();

    const { lat, lng } = event.latlng;
    this.leafletLayers.push(L.marker([lat, lng], { icon: this.stationIcon }));

    this.lat.setValue(lat);
    this.lng.setValue(lng);
  }
}
