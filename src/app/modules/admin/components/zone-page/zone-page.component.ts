import {
  Component,
  ViewChild
} from '@angular/core';
import {
  FormControl,
  Validators
} from '@angular/forms';
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
} from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Zone } from 'src/app/common/models/zone.model';
import { ZoneService } from 'src/app/common/services/zone.service';


@Component({
  selector: 'app-zone-page',
  templateUrl: './zone-page.component.html',
  styleUrls: [ './zone-page.component.css' ]
})
export class ZonePageComponent {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  private displayedColumns: string[] = ['select', 'id', 'name'];
  private dataSource: MatTableDataSource<Zone>;
  private selection = new SelectionModel<Zone>(true, []);
  private name: FormControl;
  private zone: Zone;

  public constructor(private zoneService: ZoneService) {
    this.zone = new Zone();

    this.name = new FormControl('', [Validators.required, (c: FormControl) => this.validateName(c)]);

    this.dataSource = new MatTableDataSource<Zone>([]);
    this.dataSource.filterPredicate = (zone, name) => zone.name.toLowerCase().includes(name.toLowerCase());
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.zoneService.getZones().subscribe(zones => {
      this.dataSource = new MatTableDataSource<Zone>(zones);
      this.dataSource.filterPredicate = (zone, name) => zone.name.toLowerCase().includes(name.toLowerCase());
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  }

  private save(): void {
    this.zone.name = this.name.value;

    if (this.zone.id === null) {
      this.zoneService.createZone(this.zone.name).subscribe(
        zone => {
          const data = this.dataSource.data;
          data.push(zone);
          this.dataSource.data = data;
        }
      );
    } else {
      this.zoneService.updateZone(this.zone.id, this.zone.name).subscribe(
        zone => {
          const data = this.dataSource.data;
          const index = data.findIndex(z => z.id === zone.id);
          data.splice(index, 1, zone);
          this.dataSource.data = data;
        }
      );
    }

    this.clear();
  }

  private clear(): void {
    this.zone = new Zone();
    this.name.reset();
  }

  private delete(): void {
    this.selection.selected.forEach(el => {
      this.zoneService.deleteZone(el.id).subscribe(
        zone => {
          const index = this.dataSource.data.findIndex(z => z.id === zone.id);
          const data = this.dataSource.data;
          data.splice(index, 1);
          this.dataSource.data = data;
        }
      );
    });

    this.selection.clear();
  }

  private select(element: Zone) {
    this.zone = Object.assign(new Zone(), element);
    this.name.setValue(element.name);
  }

  private applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  private validateName(c: FormControl): object {
    if (!this.dataSource) {
      return null;
    }
    const zone = this.dataSource.data.find(z => z.name === c.value);
    return zone ? { unique: 'Name not unique, please enter different name.'} : null;
  }
}
