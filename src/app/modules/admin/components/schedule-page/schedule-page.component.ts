import { ScheduleService } from 'src/app/common/services/schedule.service';
import { Component, ViewChild, OnInit, ElementRef, QueryList, EventEmitter, OnDestroy, AfterViewInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog, MatSnackBar, MatListOption, MatList, MatSelectionList } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { Line } from 'src/app/common/models/line.model';
import { Station } from 'src/app/common/models/station.model';

import 'leaflet-routing-machine';
import { LineService } from 'src/app/common/services/line.service';
import { StationService } from 'src/app/common/services/station.service';
import { Zone } from 'src/app/zones/zone.model';
import { Schedule } from 'src/app/models/schedule.model';
import { MultiViewChildren } from 'src/app/utils/decorators/multi-view-children.decorator';
import { DaysOfWeek } from 'src/app/models/days-of-week.model';


@Component({
  selector: 'app-line-page',
  templateUrl: './schedule-page.component.html',
  styleUrls: [ './schedule-page.component.css' ]
})
export class SchedulePageComponent implements OnInit, OnDestroy, AfterViewInit {


  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @MultiViewChildren(MatPaginator, { read: [ElementRef, MatPaginator] }) paginators: QueryList<any>[];
  @ViewChild(MatSelectionList) selectionList: MatSelectionList;


  private displayedLocationColumns: string[] = [];

  private dataSource: MatTableDataSource<Schedule>;
  private displayedColumns: string[] = ['key', 'routeA', 'routeB'];
  private scheduleDisplayed: Schedule;
  private locationDataSources: {};
  private selection = new SelectionModel<Schedule>(true, []);
  private expandedElement: Schedule | null;

  private snakeBarTimeout: any;
  private panelOpenState = false;
  private routeATimes: string[];
  private routeBTimes: string[];
  private routeATimeValues: string[];
  private routeBTimeValues: string[];

  private daysForMenu:DaysOfWeek[];
  private days = ['MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY','FRIDAY', 'SATURDAY','SUNDAY'];

  private lines: Line[];
  private lineControl: FormControl;
  private routeControl: FormControl;

  private line: Line;

  private stations: Station[];
  private stationsToAdd: Station[];
  private stationsToRemove: Station[];
  private currentLineSchedules: Schedule[];
  private newSchedule: Schedule;

  public constructor(
    private dialog: MatDialog,
    private lineService: LineService,
    private stationService: StationService,
    private scheduleService: ScheduleService,
    private snackBar: MatSnackBar
  ) {}

  private daysToString(days: DaysOfWeek[]) {
    return days.join(',');
  }

  private getLineDays(line: Line): any {
    if(!line) return null;
    this.daysForMenu = [];
    this.lineService.getSchedules(line.id).subscribe(
      schedules => {
        this.currentLineSchedules = schedules;
        this.currentLineSchedules.forEach( schedule => {
          let daysString = [];
          schedule.appliesTo.forEach(day => {daysString.push(DaysOfWeek[day]);});
          this.daysForMenu = [...this.daysForMenu, ...schedule.appliesTo];
        });
        
        this.daysForMenu = this.daysForMenu.filter((value, index, self)=>{
          return self.indexOf(value) === index;
        });
      });
  }

  private resetDataSource(){
    this.dataSource = null;
    this.scheduleDisplayed = null;
  }

  private getScheduleForDay(day: DaysOfWeek): Schedule {
    return this.currentLineSchedules.filter(
      schedule => {
        if (schedule.appliesTo.includes(day)) {
        }
        return schedule.appliesTo.includes(day);
      }
    )[0];
  }

  private dayChosen(day:DaysOfWeek):void{
    this.scheduleDisplayed = this.getScheduleForDay(day);
    this.dataSource = new MatTableDataSource(this.getScheduleTableData(this.scheduleDisplayed));
  }

  private getScheduleTableData(schedule: Schedule): any[] {
    const timeObjects = {};
    schedule.routeA.forEach((time) => {
      let {hours, minutes, timeString} = this.makeTimeString(time);
      timeObjects[hours] = {
        key: hours,
        routeA: timeObjects[hours] === undefined ?
          timeString : timeObjects[hours].routeA + ', ' +  timeString
      };
    });

    schedule.routeB.forEach((time) => {
      let { hours, minutes, timeString } = this.makeTimeString(time);
      timeObjects[hours] = {
        ...timeObjects[hours],
        key: hours,
        routeB: timeObjects[hours] === undefined ?
          timeString : timeObjects[hours].routeB === undefined ?
            timeString : timeObjects[hours].routeB + ',' + timeString
      };
    });
    let timeObjectsArray = [];
    for (let timeObject in timeObjects) {
      timeObjectsArray.push(timeObjects[timeObject]);
    }
    return timeObjectsArray.sort((a, b) => a.key - b.key);  }

  private onSelectA(time: string, index: number) {
    this.routeATimeValues[index] = time;
  }

  private onSelectB(time: string, index: number) {
    this.routeBTimeValues[index] = time;
  }

  private makeTimeString(time) {
    const hours = time.toString().split(':')[0];
    const minutes = time.toString().split(':')[1];
    const timeString = hours + ':' + minutes;
    return {hours, minutes, timeString};
  }

  private addRouteAInput() {
    this.routeATimes.push('00:00');
  }

  private addRouteBInput() {
    this.routeBTimes.push('00:00');
  }

  ngOnInit() {
    this.newSchedule = new Schedule();
    this.lineControl = new FormControl('', [Validators.required]);
    this.routeControl = new FormControl('', [Validators.required]);

    this.routeATimes = [];
    this.routeBTimes = [];
    this.routeATimeValues = [];
    this.routeBTimeValues = [];
    this.scheduleDisplayed = null;
    this.dataSource = new MatTableDataSource<Schedule>([]);
    this.lineService.getLines().subscribe( response => {
      this.lines = response;
    });

    this.locationDataSources = {};
    this.dataSource.sort = this.sort;

    this.line = new Line();


    this.stationService.getStations().subscribe(
      stations => {
        this.stations = stations;
      }
    );
  }

  ngAfterViewInit() {
    this.snakeBarTimeout = setTimeout(() => {
      this.snackBar.open('Click on a row to expand it!', '', {
        duration: 2000,
      });
    }, 1000);
  }

  ngOnDestroy() {
    clearTimeout(this.snakeBarTimeout);
  }

// TODO(aleksandar): custom filter function


  private removeSchedule(){
     this.scheduleService.deleteSchedule(this.scheduleDisplayed.id).subscribe(deleted => {
       this.resetDataSource();
     })
  }
  public onStartDate(event): void {
    this.newSchedule.startDate = event
  }

  public onEndDate(event): void {
    this.newSchedule.endDate = event
  }

  private save(): void {
    this.newSchedule.appliesTo = [];
    this.newSchedule.routeA = [];
    this.newSchedule.routeB = [];

    this.selectionList.selectedOptions.selected.forEach(item => {
      this.newSchedule.appliesTo.push(item.value)});
    this.routeATimeValues.forEach(time => {
      let timeArr = time.split(':');
      this.newSchedule.routeA.push(time);
    });
    this.routeBTimeValues.forEach(time => {
      let timeArr = time.split(':');
      this.newSchedule.routeB.push(time);
    });
    console.log(this.newSchedule);
    let s = this.newSchedule.startDate.toISOString().split('T')[0];
    let e = this.newSchedule.endDate.toISOString().split('T')[0];
    this.scheduleService.createSchedule(this.newSchedule.routeA, this.newSchedule.routeB, this.newSchedule.appliesTo,s,e,this.newSchedule.line).subscribe(response =>{});

  }

  private clear(): void {
    this.newSchedule = new Schedule();
    this.lineControl.reset();
    this.routeControl.reset();
  }

  private delete(): void {
    this.selection.selected.forEach(el => {
      this.lineService.deleteLine(el.id).subscribe(
        line => {
          const index = this.dataSource.data
            .findIndex(l => l.id === line.id);
          const data = this.dataSource.data;
          data.splice(index, 1);
          this.dataSource.data = data;
        }
      );
    });
  }
}
