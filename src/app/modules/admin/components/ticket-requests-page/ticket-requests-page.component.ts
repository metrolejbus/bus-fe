import { Customer } from '../../../../tickets/models/customer.model';
import { PeriodicTicketService } from '../../../../tickets/services/periodic-ticket.service';
import { Component, AfterViewInit, OnInit, Inject } from '@angular/core';
import { PeriodicTicket } from 'src/app/tickets/models/periodic-ticket.model';
import { MatTableDataSource, MatSnackBar } from '@angular/material';

import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';

@Component({
  selector: 'app-ticket-requests-page',
  templateUrl: './ticket-requests-page.component.html',
  styleUrls: [ './ticket-requests-page.component.css'],
  animations: [
    trigger('detailExpand', [
      state(
        'collapsed',
        style({ height: '0px', minHeight: '0', display: 'none' })
      ),
      state('expanded', style({ height: '*' })),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      )
    ])
  ]
})
export class TicketRequestsPageComponent implements OnInit, AfterViewInit {
  private dataSource: MatTableDataSource<PeriodicTicket>;
  private displayedColumns: string[] = [
    'id',
    'customerName',
    'duration',
    'issuedOn',
    'validTo',
    'validFrom'
  ];
  private snakeBarTimeout: any;
  private tickets = [];
  private expandedElement: PeriodicTicket | null;

  constructor(
    @Inject('BASE_API_URL') private baseUrl: string,
    public snackBar: MatSnackBar,
    private periodicTicketService: PeriodicTicketService
  ) {}

  ngOnInit() {
    this.periodicTicketService.getUnprocessedTickets().then(data => {
      this.tickets = data.map(
        ticketData => new PeriodicTicket(ticketData)
      );
      this.dataSource = new MatTableDataSource<PeriodicTicket>(this.tickets);
    });
  }

  ngAfterViewInit() {
    this.snakeBarTimeout = setTimeout(() => {
      this.snackBar.open('Click on a row to expand it!', '', {
        duration: 2000
      });
    }, 1000);
  }

  private acceptTicketRequest(ticket) {
    this.periodicTicketService
      .acceptTicketRequest(ticket)
      .then(data => {
        this.tickets = this.tickets.filter(t => t.id !== data['id']);
        this.dataSource = new MatTableDataSource<PeriodicTicket>(this.tickets);
      })
      .catch(err => console.log(err));
  }

  private rejectTicketRequest(ticket) {
    this.periodicTicketService
      .rejectTicketRequest(ticket)
      .then(data => {
        this.tickets = this.tickets.filter(t => t.id !== data['id']);
        this.dataSource = new MatTableDataSource<PeriodicTicket>(this.tickets);
      })
      .catch(err => console.log(err));
  }

  private getVerificationFileURL(customer) {
    return this.baseUrl + '/storage/' + customer.ticketVerificationFileName;
  }

  private applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
}

