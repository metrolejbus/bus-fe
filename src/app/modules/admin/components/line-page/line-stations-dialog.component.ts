import {
  Component,
  OnInit,
  Inject,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import {
  CdkDragDrop,
  moveItemInArray,
} from '@angular/cdk/drag-drop';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
  MatAutocompleteSelectedEvent,
} from '@angular/material';

import * as L from 'leaflet';
import 'leaflet-routing-machine';

import { Station } from 'src/app/common/models/station.model';
import { Line } from 'src/app/common/models/line.model';
import { LineService } from 'src/app/common/services/line.service';
import {
  MAP_ICONS,
  MAP_ICONS_COLOR,
} from 'src/app/constants';

@Component({
  selector: 'app-line-stations-dialog',
  templateUrl: './line-stations-dialog.component.html',
  styleUrls: [ './line-stations-dialog.component.css' ]
})
export class LineStationDialogComponent implements OnInit {
  private stationControl: FormControl;
  private showRemoveBtn: {};

  private line: Line;

  private availableStations: Station[];
  private chosenStations: Station[];
  private filteredStations: Station[];

  private leafletOptions: L.MapOptions;
  private leafletLayers: L.Layer[] = [];
  private leafletMap: L.Map;
  private routingControl: L.Routing.Control;
  private routingPlan: L.Routing.Plan;

  private stationIcon: L.DivIcon;
  private dotIcon: L.DivIcon;

  private clickedPopup: L.Popup;

  ngOnInit() {
    this.stationControl = new FormControl();
    this.stationControl.valueChanges.subscribe((value: string) => this.filterStations(value));

    this.showRemoveBtn = {};
  }

  constructor(
    public dialogRef: MatDialogRef<LineStationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: {
      stations: Station[],
      line: Line,
      options: {
        map: L.MapOptions,
        router: L.Routing.IRouter;
      }
    },
  ) {
    this.leafletOptions = this.data.options.map;
    this.line = data.line;

    this.availableStations = [];
    for (const station of data.stations) {
      if (data.line.stations.findIndex(s => s.id === station.id) === -1) {
        this.availableStations.push(station);
      }
    }
    this.filteredStations = this.availableStations;

    this.chosenStations = [];
    data.line.stations.forEach(s => this.chosenStations.push(s));

    this.dotIcon = L.divIcon({
      html: `<i class="material-icons" style="font-size: 10px;">fiber_manual_record</i>`,
      iconSize: [12, 12],
    });

    const lineType = this.line.type.toUpperCase();

    const iconHTML = `
    <div style="
      border-radius: 50%;
      background: ${MAP_ICONS_COLOR[lineType]};
      color: white;
      width: 24px;
      height: 24px;
      display: flex;
      justify-content: center;
      align-items: center;"
    >
      <i class="material-icons material-icons.md-12">${MAP_ICONS[lineType]}</i>
    </div>
    `;

    this.stationIcon = L.divIcon({
      html: iconHTML,
      iconSize: [18, 18],
    });

    let waypoints = null;
    // Configure routing
    if (this.line.route) {
      const route = LineService.routeFromString(this.line.route);
      waypoints = route.map(wp => L.latLng(wp.lat, wp.lng));
    }

    this.routingPlan = L.Routing.plan(waypoints || [], {
      createMarker: (_, wp) => {
        const { lat, lng } = wp.latLng;
        const station =
          this.chosenStations.find(
            s => s.location.lat === lat && s.location.lng === lng
          );

        if (station) {
          return L.marker(wp.latLng, {
            icon: this.stationIcon,
            draggable: false,
            clickable: true,
          }).bindPopup(station.name);
        }

        const removeLink = document.createElement('a');
        removeLink.style.cssText = 'cursor: pointer';
        removeLink.text = 'Remove';
        removeLink.onclick = () => this.removeMarker();

        return L.marker(wp.latLng, {
          icon: this.dotIcon,
          draggable: true,
          clickable: false,
        }).bindPopup(removeLink);
      },
    });

    this.routingControl = L.Routing.control({
      plan: this.routingPlan,
      router: data.options.router,
      lineOptions: {
        styles: [{ color: MAP_ICONS_COLOR[lineType] }],
      }
    });
  }

  private dismiss(): void {
    this.dialogRef.close();
  }

  private apply(): void {
    const removed: Station[] = [];
    const added: Station[] = [];

    const chosen: Station[] = this.chosenStations;

    for (const station of chosen) {
      if (this.line.stations.findIndex(s => s.id === station.id) === -1) {
        added.push(station);
      }
    }

    for (const station of this.line.stations) {
      if (chosen.findIndex(s => s.id === station.id) === -1) {
        removed.push(station);
      }
    }

    const route: { lat: number, lng: number, station: boolean }[] = [];

    for (const wp of this.routingPlan.getWaypoints()) {
      const { lat, lng } = wp.latLng;
      const station = chosen.findIndex(
        s => s.location.lat === lat && s.location.lng === lng
      ) === -1;
      route.push({ lat, lng, station });
    }

    this.dialogRef.close({ added, removed, route: LineService.routeToString(route) });
  }

  private getStationName(station?: Station): string {
    return station ? `${station.name},${station.zone.name}` : undefined;
  }

  private filterStations(value: any): void {
    if (typeof value === 'object') {
      return;
    }

    const filterValue = value.toLowerCase();
    this.filteredStations = this.availableStations.filter(
      state => state.name.toLowerCase().indexOf(filterValue) === 0
    );
  }

  private removeMarker(): void {
    const index = this.routingPlan
      .getWaypoints()
      // @ts-ignore
      .findIndex(wp => wp.latLng === this.clickedPopup._source.getLatLng());
    this.routingPlan.spliceWaypoints(index , 1);
    this.clickedPopup.closePopup();
  }

  private onSelect(event: MatAutocompleteSelectedEvent): void {
    const station: Station = event.option.value;

    this.availableStations.splice(this.availableStations.indexOf(station), 1);
    this.chosenStations.push(station);

    this.stationControl.reset();

    const wp = L.Routing.waypoint(
      L.latLng(station.location.lat, station.location.lng)
    );

    if (this.chosenStations.length === 1) {
      this.routingPlan.setWaypoints([wp]);
    } else if (this.chosenStations.length === 2) {
      this.routingPlan.spliceWaypoints(-1, 1, wp);
    } else {
      this.routingPlan.spliceWaypoints(-1, 0, wp);
    }

    this.filteredStations = this.availableStations;
  }

  private onRemove(index: number, station: Station): void {
    this.chosenStations.splice(index, 1);

    const idx = this.routingPlan
      .getWaypoints()
      .findIndex(
        wp => wp.latLng.lat === station.location.lat &&
              wp.latLng.lng === station.location.lng
      );

    this.routingPlan.spliceWaypoints(idx, 1);

    this.availableStations.push(station);
    this.availableStations.sort((a, b) => a.name < b.name ? -1 : 1);
  }

  private onMapReady(leafletMap: L.Map): void {
    this.leafletMap = leafletMap;
    this.routingControl.addTo(this.leafletMap);

    this.leafletMap.on('popupopen', (e: L.PopupEvent) => {
      this.clickedPopup = e.popup;
    });
  }

  private onDrop(event: CdkDragDrop<string[]>): void {
    const s1: Station = this.chosenStations[event.previousIndex];
    const s2: Station = this.chosenStations[event.currentIndex];

    const waypoints = this.routingPlan.getWaypoints();

    const index1 = waypoints
      .findIndex(
        wp => wp.latLng.lat === s1.location.lat && wp.latLng.lng === s1.location.lng
      );

    const index2 = waypoints
      .findIndex(
        wp => wp.latLng.lat === s2.location.lat && wp.latLng.lng === s2.location.lng
      );

    const wp1 = waypoints[index1];
    const wp2 = Object.assign({}, waypoints[index2]);

    this.routingPlan.spliceWaypoints(index2, 1, wp1);
    this.routingPlan.spliceWaypoints(index1, 1, wp2);

    moveItemInArray(this.chosenStations, event.previousIndex, event.currentIndex);
  }
}
