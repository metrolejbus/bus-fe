import {
  Component,
  ViewChild,
  OnInit,
} from '@angular/core';
import {
  FormControl,
  Validators,
} from '@angular/forms';
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatDialog,
} from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';

import * as L from 'leaflet';
import 'leaflet-routing-machine';

import { Line } from 'src/app/common/models/line.model';
import { Station } from 'src/app/common/models/station.model';
import { Zone } from 'src/app/zones/zone.model';
import { LineService } from 'src/app/common/services/line.service';
import { StationService } from 'src/app/common/services/station.service';
import { LineStationDialogComponent } from './line-stations-dialog.component';


@Component({
  selector: 'app-line-page',
  templateUrl: './line-page.component.html',
  styleUrls: [ './line-page.component.css' ]
})
export class LinePageComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  private types = ['BUS', 'METRO', 'TRAM'];

  private displayedColumns: string[] = ['select', 'id', 'name', 'type'];
  private dataSource: MatTableDataSource<Line>;
  private selection = new SelectionModel<Line>(true, []);

  private name: FormControl;
  private type: FormControl;

  private line: Line;

  private stations: Station[];
  private stationsToAdd: Station[];
  private stationsToRemove: Station[];

  public constructor(
    private dialog: MatDialog,
    private lineService: LineService,
    private stationService: StationService,
  ) {

  }

  ngOnInit() {
    this.line = new Line();

    this.name = new FormControl('', [Validators.required, Validators.maxLength(4)]);
    this.type = new FormControl('', [Validators.required]);

    this.dataSource = new MatTableDataSource<Line>([]);
    this.dataSource.filterPredicate = (line, name) => line.name.toLowerCase().includes(name.toLowerCase());

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

    this.lineService.getLines().subscribe(
      lines => {
        this.dataSource = new MatTableDataSource<Line>(lines);
        this.dataSource.filterPredicate = (line, name) => line.name.toLowerCase().includes(name.toLowerCase());
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    );

    this.stationService.getStations().subscribe(
      stations => {
        this.stations = stations;
      }
    );

  }

  private save(): void {
    this.line.name = this.name.value;
    this.line.type = this.type.value;

    if (this.line.id === null) {
      this.lineService.createLine(
        this.line.name, this.line.type, this.line.route,
      ).subscribe(
        line => {
          const data = this.dataSource.data;
          data.push(line);
          this.dataSource.data = data;
          const stationIds = this.stationsToAdd.map(s => s.id);

          this.lineService.addStations(line.id, stationIds).subscribe(
            line2 => {
              this.line.stations = line2.stations;
            }
          );
        }
      );
    } else {
      this.lineService.updateLine(
        this.line.id, this.line.name, this.line.route
      ).subscribe(
        line => {
          const data = this.dataSource.data;
          const index = data.findIndex(s => s.id === line.id);
          data.splice(index, 1, line);
          this.dataSource.data = data;
        }
      );
    }

    this.clear();
  }

  private clear(): void {
    this.line = new Line();
    this.name.reset();
    this.type.reset();
  }

  private delete(): void {
    this.selection.selected.forEach(el => {
      this.lineService.deleteLine(el.id).subscribe(
        line => {
          const index = this.dataSource.data
            .findIndex(l => l.id === line.id);
          const data = this.dataSource.data;
          data.splice(index, 1);
          this.dataSource.data = data;
        }
      );
    });

    this.selection.clear();
  }

  private select(element: Line) {
    this.line = Object.assign(new Zone(), element);
    this.name.setValue(element.name);
    this.type.setValue(element.type);
  }

  private openStationsDialog(): void {
    const dialogRef = this.dialog.open(LineStationDialogComponent, {
      width: '900px',
      height: '650px',
      data: {
        stations: this.stations,
        line: this.line,
        options: {
          map: {
            zoom: 14,
            center: L.latLng(51.5076, -0.1177),
            layers: [
              L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                minZoom: 11,
                maxZoom: 18,
                attribution: '...'
              }),
            ],
          },
          router: L.Routing.osrmv1({ serviceUrl: 'http://localhost:5000/route/v1'})
        }
      },
      autoFocus: false,
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.line.route = result.route;
        this.stationsToAdd = result.added;
        this.stationsToRemove = result.removed;
      }
    });
  }

  private openSchedulesDialog(): void {
  }

  private applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
