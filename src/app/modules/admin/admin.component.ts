import {
  Component,
  OnInit
} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: [ './admin.component.css' ]
})
export class AdminComponent implements OnInit {
  private routes: Route[] = [
    { path: 'zones', label: 'Zones' },
    { path: 'stations', label: 'Stations' },
    { path: 'lines', label: 'Lines' },
    { path: 'ticket-requests', label: 'Ticket Requests' },
    { path: 'schedules', label: 'Schedules' },
    { path: 'ticket-prices', label: 'Ticket prices' }
  ];

  public constructor(private router: Router) {

  }

  ngOnInit() {
    this.router.navigate(['/admin/zones']);
  }
}

interface Route {
  path: string;
  label: string;
}
