import { TicketRequestsPageComponent } from './components/ticket-requests-page/ticket-requests-page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { AppMaterialModule } from 'src/app/app-material/app-material.module';
import { AdminComponent } from './admin.component';
import { ZonePageComponent } from './components/zone-page/zone-page.component';
import { StationPageComponent } from './components/station-page/station-page.component';
import { LinePageComponent } from './components/line-page/line-page.component';
import { SchedulePageComponent } from './components/schedule-page/schedule-page.component';

import { AdminRoutingModule } from './admin-routing.module';
import { LineStationDialogComponent } from './components/line-page/line-stations-dialog.component';
import { LineSchedulesDialogComponent } from './components/line-page/line-schedules-dialog.component';
import { StationMapDialogComponent } from './components/station-page/station-map-dialog.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MatMenuModule } from '@angular/material/menu';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { TicketPricePageComponent } from './components/ticket-price-page/ticket-price-page.component';

@NgModule({
  declarations: [
    AdminComponent,
    LinePageComponent,
    LineSchedulesDialogComponent,
    LineStationDialogComponent,
    StationMapDialogComponent,
    StationPageComponent,
    TicketRequestsPageComponent,
    ZonePageComponent,
    SchedulePageComponent,
    TicketPricePageComponent
  ],
  entryComponents: [
    LineSchedulesDialogComponent,
    LineStationDialogComponent,
    StationMapDialogComponent,
  ],
  imports: [
    AppMaterialModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    LeafletModule,
    DragDropModule,

    AdminRoutingModule,
    MatMenuModule,
    ScrollDispatchModule,
    NgxMaterialTimepickerModule.forRoot()
  ],
  exports: [
    AdminComponent,
  ]
})
export class AdminModule {
}
