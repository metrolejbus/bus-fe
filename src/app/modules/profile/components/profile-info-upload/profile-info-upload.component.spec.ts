import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileInfoUploadComponent } from './profile-info-upload.component';

describe('ProfileInfoUploadComponent', () => {
  let component: ProfileInfoUploadComponent;
  let fixture: ComponentFixture<ProfileInfoUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileInfoUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileInfoUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
