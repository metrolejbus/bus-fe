import {ProfileService} from './../../../../common/services/profile.service';
import {Component, Inject, OnInit} from '@angular/core';

@Component({
  selector: 'app-profile-info-upload',
  templateUrl: './profile-info-upload.component.html',
  styleUrls: ['./profile-info-upload.component.css']
})
export class ProfileInfoUploadComponent implements OnInit {
  constructor(private profileService: ProfileService,
              @Inject('BASE_API_URL') private baseUrl: string,
  ) {
  }

  fileToUpload: File = null;
  customer = null;
  imagePreview = null;

  ngOnInit() {
    this.fetchCustomer();
  }

  async fetchCustomer() {
    this.customer = JSON.parse(localStorage.getItem('user')).customer;
    this.customer = await this.profileService.fetchProfileInfo();
  }

  handleFileInput(input) {
    input = input.target;
    this.fileToUpload = input.files[0];
    if (input.files && input.files[0]) {
      const reader = new FileReader();

      reader.onload = (e) => {
        this.imagePreview = e.target.result;
      };

      reader.readAsDataURL(input.files[0]);
    }
  }

  private getVerificationFileURL() {
    return this.baseUrl + '/storage/' + this.customer.ticketVerificationFileName;
  }


  uploadProfileVerification() {
    this.profileService.uploadProfileVerificationInfo(this.fileToUpload)
      .then(data => {
        this.customer = data;
        this.fileToUpload = null;
        console.log('customer', this.customer);
        console.log(Boolean(this.fileToUpload));
      })
      .catch(error => {
        console.error(error);
      });
  }
}
