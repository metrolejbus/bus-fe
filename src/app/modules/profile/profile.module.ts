import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {ProfileInfoUploadComponent} from './components/profile-info-upload/profile-info-upload.component';
import {ModuleRouting} from './profile-routing.module';
import { AppMaterialModule } from './../../app-material/app-material.module';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ProfileInfoUploadComponent
  ],
  imports: [
    ModuleRouting,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    AppMaterialModule

  ],
})
export class ProfileModule { }
