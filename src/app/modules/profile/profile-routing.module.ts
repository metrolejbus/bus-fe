import {ModuleWithProviders, NgModule} from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import {ProfileInfoUploadComponent} from './components/profile-info-upload/profile-info-upload.component';


const routes: Routes = [
  {
    path: 'profile',
    children: [
      { path: '', redirectTo: 'profile-info', pathMatch: 'full' },
      { path: 'profile-info', component: ProfileInfoUploadComponent },
    ]
  }
];


@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule,
  ]
})
export class ProfileRoutingModule {
}

export const ModuleRouting: ModuleWithProviders = RouterModule.forChild(routes);
