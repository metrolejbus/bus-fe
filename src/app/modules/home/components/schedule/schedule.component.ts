import {
  Component,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import {
  MatPaginator,
  MatSort,
  MatTableDataSource,
  MatSnackBar
} from '@angular/material';

import { DaysOfWeek } from 'src/app/models/days-of-week.model';
import { Line } from 'src/app/common/models/line.model';
import { Schedule } from 'src/app/models/schedule.model';

import { SelectionModel } from '@angular/cdk/collections';
import { LineService } from 'src/app/common/services/line.service';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: [ './schedule.component.css' ]
})
export class ScheduleComponent {

  @Output() lineFocused: EventEmitter<Line> = new EventEmitter<Line>();

  private selectedLines: Line[];

  private dataSource: MatTableDataSource<Object[]>;
  private selection = new SelectionModel<Schedule>(true, []);
  private displayedColumns: string[] = ['key', 'routeA', 'routeB'];
  private selectedType: string;

  private lineSet: Line[];
  private lineSetIndex: number;

  private hasNextLineSet: boolean;
  private hasPreviousLineSet: boolean;
  private longestNameLength: number;

  private days: string[];
  private scheduleIndex: number;
  private currentLineSchedules: Schedule[];
  private emitTimeout: any;
  private selectedDay: DaysOfWeek;
  @Input() schedules: Schedule[];


  public constructor(private lineService: LineService) {
    this.selectedLines = [];
    this.hasPreviousLineSet = false;
    this.hasNextLineSet = false;
    this.currentLineSchedules = [];
  }

  private daysToString(days: DaysOfWeek[]) {
    return days.join(',');
  }

  @Input() set lines(data: Line[]) {
    if (data.length === this.selectedLines.length) {
      return;
    }
    this.selectedLines = data;
    this.scheduleIndex = 0;
    this.lineSetIndex = 0;
    this.hasNextLineSet = this.selectedLines.length > 1;
    this.hasPreviousLineSet = false;
    if (this.selectedLines[0]) {
      this.getSchedules(this.selectedLines[0]);
    }
    if (this.selectedLines.length >= 1) {
      this.lineSet = this.generateLinesSet();
      this.selectedType = this.selectedLines[0].type;
    } else {
      this.lineSet = null;
    }

    let longest = 0;
    this.selectedLines.forEach(line => {
      if (line.name.length > longest) {
        longest = line.name.length;
      }
    });
    this.longestNameLength = longest;
  }

  private getSchedules(line: Line) {
    this.lineService.getSchedules(line.id).subscribe(
      schedules => {
        this.currentLineSchedules = schedules;
        this.days = this.getLineDays(this.selectedLines[0]);
        if (this.currentLineSchedules[0]) {
          this.dataSource = new MatTableDataSource(this.getScheduleTableData(this.currentLineSchedules[0]));
        }
      }
    );
  }

  private getLineDays(line: Line): any {
    let days = [];
    this.currentLineSchedules.forEach( schedule => {
      const daysString = [];
      schedule.appliesTo.forEach(day => daysString.push(DaysOfWeek[day]));
      days = [...days, ...schedule.appliesTo];
    });
    return days.filter((value, index, self) => {
      return self.indexOf(value) === index;
    });
  }

  private getScheduleForDay(day: DaysOfWeek): Schedule {
    return this.currentLineSchedules.filter(
      schedule => {
        if (schedule.appliesTo.includes(day)) {

        }
        return schedule.appliesTo.includes(day);
      }
    )[0];
  }

  private dayChosen(day: DaysOfWeek): void {
    const scheduleForDay = this.getScheduleForDay(day);
    this.dataSource = new MatTableDataSource(this.getScheduleTableData(scheduleForDay));
    this.selectedDay = day;
  }

  private getScheduleTableData(schedule: Schedule): any[] {
    const timeObjects = [];
    const niz = [];
    schedule.routeA.forEach((time) => {
      const { hours, minutes, timeString } = this.makeTimeString(time);
      niz.push(timeString);
      timeObjects[hours] = {
        key: hours,
        routeA: timeObjects[hours] === undefined ?
          timeString : timeObjects[hours].routeA + ', ' +  timeString
      };
    });

    schedule.routeB.forEach((time) => {
      const {hours, minutes, timeString} = this.makeTimeString(time);
      timeObjects[hours] = {
        ...timeObjects[hours],
        key: hours,
        routeB: timeObjects[hours] === undefined ?
          timeString : timeObjects[hours].routeB === undefined ?
            timeString : timeObjects[hours].routeB + ',' + timeString
      };
    });
    const x = timeObjects.filter(element => element !== undefined);
    return x;
  }

  private makeTimeString(time) {
    const hours = time.toString().split(':')[0];
    const minutes = time.toString().split(':')[1];
    const timeString = hours + ':' + minutes;
    return { hours, minutes, timeString };
  }

  private nextLineSet(): void {
    this.lineSetIndex += 1;
    this.selectedDay = null;

    this.getSchedules(this.selectedLines[this.lineSetIndex]);

    this.lineSet = this.generateLinesSet();
    if (this.lineSetIndex + 1 === this.selectedLines.length) {
      this.hasNextLineSet = false;
    }
    this.hasPreviousLineSet = true;
  }

  private previousLineSet(): void {
    this.lineSetIndex -= 1;
    this.selectedDay = null;
    this.getSchedules(this.selectedLines[this.lineSetIndex]);

    this.lineSet = this.generateLinesSet();
    if (this.lineSetIndex - 1 === -1) {
      this.hasPreviousLineSet = false;
    }
    this.hasNextLineSet = true;
  }

  private generateLinesSet(): Line[] {
    clearTimeout(this.emitTimeout);

    const n = this.selectedLines.length;
    const idx = this.lineSetIndex - 1;
    const ret: Line[] = [null, null, null];

    for (let i = 0; i < 3; i++) {
      if (idx + i >= 0 && idx + i < n) {
        ret[i] = this.selectedLines[idx + i];
      }
    }

    this.lineFocused.emit(ret[1]);

    return ret;
  }

  private selectedTypes(): string[] {
    const types = {};
    for (const line of this.selectedLines) {
      if (line.type in types) {
        continue;
      }
      types[line.type] = null;
    }
    return Object.keys(types);
  }
}
