import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { Line } from 'src/app/common/models/line.model';

@Component({
  selector: 'app-line-picker',
  templateUrl: './line-picker.component.html',
  styleUrls: [ './line-picker.component.css' ]
})
export class LinePickerComponent {

  @Input() title = 'Title';
  @Input() collapsed = false;

  @Output() select: EventEmitter<Line> = new EventEmitter<Line>();
  @Output() deselect: EventEmitter<Line> = new EventEmitter<Line>();

  private elements: LineSelect[];

  public constructor() {
    this.elements = [];
  }

  @Input() set lines(data: Line[]) {
    const elements = data.map(line => ({ line, active: false }));

    if (elements.length !== this.elements.length) {
      this.elements = elements;
    }
  }

  private onClick(element: LineSelect): void {
    const { line, active } = element;
    active ? this.deselect.emit(line) : this.select.emit(line);
    element.active = !active;
  }
}

interface LineSelect {
  line: Line;
  active: boolean;
}
