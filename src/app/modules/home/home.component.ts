import {
  Component,
  OnInit
} from '@angular/core';
import {
  Map,
  tileLayer,
} from 'leaflet';

import * as L from 'leaflet';
import 'leaflet-routing-machine';

import {
  MAP_ICONS,
  MAP_ICONS_COLOR,
} from 'src/app/constants';
import { Line } from 'src/app/common/models/line.model';
import { LineService } from 'src/app/common/services/line.service';
import { DaysOfWeek } from 'src/app/models/days-of-week.model';
import { JourneyService } from 'src/app/common/services/journey.service';
import { TimedLocation } from 'src/app/common/models/timed-location.model';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  private types = ['BUS', 'METRO', 'TRAM'];
  private selectedLines: {};
  private lines: Line[];

  private leafletMap: Map;
  private leafletControls: {};
  private leafletFocused: { line: Line, control: L.Routing.Control };
  private lineSchedules: {};
  private days: DaysOfWeek[];
  private leafletJourneyIndicators: object;

  private leafletOptions = {
    zoom: 14,
    center: L.latLng(51.5076, -0.1177),
    layers: [
      tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' })
    ],
  };

  public constructor(
    private lineService: LineService,
    private journeyService: JourneyService,
  ) {
    this.lines = [];
    this.leafletControls = {};
    this.leafletJourneyIndicators = {};
  }

  ngOnInit() {
    this.selectedLines = {};
    this.lineSchedules = {};
    this.lineService.getLines().subscribe(
      data => {
        this.lines = data;
      }
    );

    this.journeyService.locations.subscribe((location) => this.onLocation(location));
    this.journeyService.journeyEnds.subscribe((journeyId) => this.onJourneyEnd(journeyId));
  }

  private getLines(type: string): Line[] {
    return this.lines.filter(line => line.type === type);
  }

  private getSelectedLines(): Line[] {
    return Object.values(this.selectedLines);
  }

  private onSelect(line: Line): void {
    this.selectedLines[line.id] = line;

    const control = this.createControl(line);
    this.leafletControls[line.id] = control;
    control.addTo(this.leafletMap);

    if (this.leafletFocused) {
      this.leafletMap.removeControl(this.leafletFocused.control);
      this.leafletFocused.control.addTo(this.leafletMap);
    }
  }

  private onDeselect(line: Line): void {
    delete this.selectedLines[line.id];
    this.leafletMap.removeControl(this.leafletControls[line.id]);
    delete this.leafletControls[line.id];

    if (this.leafletFocused.line.id === line.id) {
      this.leafletMap.removeControl(this.leafletFocused.control);
      this.leafletFocused = null;
    }
  }

  private onLocation(location: TimedLocation) {
    if (!this.leafletFocused) {
      return;
    }

    const { lineId, journeyId, lat, lng, timestamp } = location;

    // step 1: see if line is focused
    if (this.leafletFocused.line.id !== lineId) {
      return;
    }

    // step 2: remove old vehicle indicator
    const oldIndicator = this.leafletJourneyIndicators[journeyId];
    if (oldIndicator) {
      this.leafletMap.removeLayer(oldIndicator);
    }

    // step 3: add new indicator
    const timeInfo = document.createElement('p');
    timeInfo.style.cssText = 'cursor: pointer; text-align: center';
    timeInfo.innerText = location.timestamp.substr(11, 5);

    const indicator = L.marker(L.latLng(lat, lng), {
      icon: L.divIcon({
        html: `
          <div style="
            border-radius: 50%;
            background: black;
            color: white;
            width: 16px;
            height: 16px;
            display: flex;
            justify-content: center;
            align-items: center;"
          >
            <i class="material-icons" style="font-size: 12px">trip_origin</i>
          </div>
        `,
        iconSize: [16, 16],
      }),
      draggable: false,
      clickable: false,
    }).bindPopup(timeInfo);

    indicator.setZIndexOffset(600);
    indicator.addTo(this.leafletMap);

    this.leafletJourneyIndicators[journeyId] = indicator;
  }

  private onJourneyEnd(journeyId: number): void {
    const indicator = this.leafletJourneyIndicators[journeyId];
    if (indicator) {
      this.leafletMap.removeLayer(indicator);
    }
  }

  private onLineFocused(line: Line): void {
    // Same line is focused -> do nothing
    if (this.leafletFocused && this.leafletFocused.line.id === line.id) {
      return;
    }

    if (this.leafletFocused) {
      // Remove previously focused line
      this.leafletMap.removeControl(this.leafletFocused.control);
      // Remove journey indicators
      for (const indicator of Object.values(this.leafletJourneyIndicators)) {
        this.leafletMap.removeLayer(indicator);
      }

      // Create grey route for previously focused line
      const prevControl = this.createControl(this.leafletFocused.line);
      this.leafletControls[this.leafletFocused.line.id] = prevControl;
      prevControl.addTo(this.leafletMap);
    }
    // Remove grey route for selected line
    this.leafletMap.removeControl(this.leafletControls[line.id]);

    // // Create new focused route for selected line
    const control = this.createControl(line, true);
    this.leafletControls[line.id] = control;
    control.addTo(this.leafletMap);
    this.leafletFocused = { line, control };
  }

  private onMapReady(leafletMap: Map): void {
    this.leafletMap = leafletMap;
  }

  private createControl(line: Line, focused: boolean = false): L.Routing.Control {
    const routes = LineService.routeFromString(line.route);
    const waypoints = routes.map(route => L.latLng(route.lat, route.lng));

    let createMarker: (index: number, wp: L.Routing.Waypoint, num: number) => L.Marker<any> = () => null;
    let styles: any = [{
      color: '#757575',
      weight: 7,
    }];

    if (focused) {
      createMarker = (_, wp) => {
        const { lat, lng } = wp.latLng;
        const index = routes.findIndex(p => p.lat === lat && p.lng === lng);

        if (!routes[index] || !routes[index].station) {
          return null;
        }

        return L.marker(wp.latLng, {
          icon: this.getIcon(line.type),
          draggable: false,
          clickable: false,
        });
      };

      styles = [{
        color: MAP_ICONS_COLOR[line.type],
        weight: 9,
      }];
    }

    const plan = L.Routing.plan(waypoints, {
      createMarker,
      addWaypoints: false,
    });

    return L.Routing.control({
      plan,
      routeWhileDragging: false,
      router: L.Routing.osrmv1({ serviceUrl: 'http://localhost:5000/route/v1'}),
      lineOptions: { styles, addWaypoints: false }
    });
  }

  private getIcon(type: string): L.DivIcon {
    const iconHTML = `
      <div style="
        border-radius: 50%;
        background: ${MAP_ICONS_COLOR[type]};
        color: white;
        width: 24px;
        height: 24px;
        display: flex;
        justify-content: center;
        align-items: center;"
      >
        <i class="material-icons material-icons.md-12">${MAP_ICONS[type]}</i>
      </div>
    `;

    return L.divIcon({
      html: iconHTML,
      iconSize: [18, 18],
    });
  }
}
