import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AppMaterialModule } from 'src/app/app-material/app-material.module';

import { HomeComponent } from './home.component';
import { LinePickerComponent } from './components/line-picker/line-picker.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { MatMenuModule } from '@angular/material/menu';

@NgModule({
  declarations: [
    HomeComponent,
    LinePickerComponent,
    ScheduleComponent,
  ],
  imports: [
    AppMaterialModule,
    CommonModule,
    LeafletModule,
    MatMenuModule
  ],
  exports: [ HomeComponent ]
})
export class HomeModule {
}
