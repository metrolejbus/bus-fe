import { Router, NavigationStart } from '@angular/router';
import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private title = 'metrolejbus';

  private authenticated = false;
  private admin = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private ngZone: NgZone,
  ) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.updatePermissions();
      }
    });
  }

  ngOnInit() {
    this.updatePermissions();
  }

  private updatePermissions(): void {
    this.authenticated = this.authService.isAuthenticated();
    this.admin = this.authService.isAdmin();
  }

  private navigate(route: string): void {
    this.router.navigate([route]);
  }

  private logout(): void {
    this.authService.logout();
    this.admin = false;
    this.navigate('/auth/login');
  }
}
