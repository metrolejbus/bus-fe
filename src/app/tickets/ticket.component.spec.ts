import { TicketComponent } from "./ticket.component";
import { TestBed, ComponentFixture } from "@angular/core/testing";
import { Router } from "@angular/router";
import { AppMaterialModule } from "../app-material/app-material.module";

describe('TicketComponent', () => {
  let component: TicketComponent;
  let fixture: ComponentFixture<TicketComponent>;
  let router: any;

  beforeEach(() => {
    let routerMock = {
      navigate: jasmine.createSpy('navigate')
    };

    TestBed.configureTestingModule({
      declarations: [ TicketComponent ],
      providers: [ { provide: Router, useValue: routerMock } ],
      imports: [
        AppMaterialModule
      ]
    });

    fixture = TestBed.createComponent(TicketComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
  });

  it('should navigate to ticket types page', () => {
    component.onBuyOnlineButtonClick();
    expect(router.navigate).toHaveBeenCalledWith(['/tickets/types']);
  });

  it('should navigate to purchased tickets page', () => {
    component.onSeeTicketsButtonClick();
    expect(router.navigate).toHaveBeenCalledWith(['/tickets/purchased']);
  });

});