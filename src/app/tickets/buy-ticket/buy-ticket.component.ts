import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';
import { ZoneService } from 'src/app/zones/zone.service';
import { Zone } from 'src/app/zones/zone.model';
import { HttpClient } from '@angular/common/http';
import { SingleTicketService } from './../services/single-ticket.service';
import { PeriodicTicketService } from './../services/periodic-ticket.service';
import { MatDatepickerInputEvent, MatSnackBar, MatDialog } from '@angular/material';
import { DailyTicketService } from './../services/daily-ticket.service';

@Component({
  selector: 'app-buy-ticket-component',
  templateUrl: './buy-ticket.component.html'
  // styleUrls: [ './buy-ticket-component.css' ]
})
export class BuyTicketComponent implements OnInit {
  constructor(
    private activatedRoute: ActivatedRoute,
    private zoneService: ZoneService,
    private router: Router,
    private http: HttpClient,
    private singleTicketService: SingleTicketService,
    private periodicTicketService: PeriodicTicketService,
    private dailyTicketService: DailyTicketService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog
  ) {
    this.type = activatedRoute.snapshot.params['type'];
  }

  type: string;
  zones: Zone[];
  selectedZone: Zone;
  createdTicket;
  durations = ['month', 'year'];
  selectedDuration: string;
  selectedDate;
  minDate;

  ngOnInit() {
    this.getZones();
    this.minDate = new Date();
  }

  getZones(): void {
    this.zoneService.getZones().subscribe(zones => (this.zones = zones));
  }

  openDialog() {
    const dialogRef = this.dialog.open(ConfirmPurchaseDialog);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.onBuyButton();
      }
    });
  }

  onBuyButton() {
    switch (this.type) {
      case 'single':
        this.singleTicketService
          .createTicket(this.selectedZone)
          .subscribe(result => {
            this.createdTicket = result;
            this.snackBar.open("Successfully purchased", "", {
              duration: 4000,
            });
          },
          err => {
            this.snackBar.open("Error", "", {
              duration: 4000,
            });
          });
        break;
      case 'periodic':
        this.periodicTicketService
          .createTicket(
            this.selectedZone,
            this.selectedDate,
            this.selectedDuration
          )
          .subscribe(createdTicket => {
            this.createdTicket = createdTicket;
            this.snackBar.open("Successfully purchased", "", {
              duration: 4000,
            });
          },
          err => {
            this.snackBar.open("Error", "", {
              duration: 4000,
            });
          });
        break;
      default:
        this.dailyTicketService
          .createTicket(this.selectedZone, this.selectedDate)
          .subscribe(createdTicket => {
            this.createdTicket = createdTicket;
            this.snackBar.open("Successfully purchased", "", {
              duration: 4000,
            });
          },
          err => {
            this.snackBar.open("Error", "", {
              duration: 4000,
            });
          });
        break;
    }
  }

  onDateInput(event: MatDatepickerInputEvent<Date>) {
    const date: string[] = event.value.toLocaleDateString().split('/');
    this.selectedDate = `${date[1]}.${date[0]}.${date[2]}.`;
  }

  showPurchasedTickets() {
    this.router.navigate(['tickets/purchased']);
  }
}

@Component({
  selector: 'confirm-purchase-dialog',
  templateUrl: 'confirm-purchase-dialog.html',
})
export class ConfirmPurchaseDialog {}
