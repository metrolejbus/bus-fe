// import { ComponentFixture, TestBed } from "@angular/core/testing";
// import { Router, RouterModule } from "@angular/router";
// import { AppMaterialModule } from "src/app/app-material/app-material.module";
// import { BuyTicketComponent } from "./buy-ticket.component";
// import { routes } from './../../app-routing.module';
// import { HomeComponent } from "src/app/modules/home/home.component";
// import { TicketComponent } from "../ticket.component";
// import { TicketTypesComponent } from "../ticket-types/ticket-types.component";
// import { PurchasedTicketsComponent } from "../purchased/purchased-tickets.component";
// import { TicketTypeInfoComponent } from "../ticket-type-info/ticket-type-info.component";
// import { AppRoutingModule } from "src/app/auth/auth-routing.module";

// describe('BuyTicketComponent', () => {
//   let component: BuyTicketComponent;
//   let fixture: ComponentFixture<BuyTicketComponent>;
//   let router: any;

//   beforeEach(() => {
//     let routerMock = {
//       navigate: jasmine.createSpy('navigate')
//     };

//     TestBed.configureTestingModule({
//       declarations: [
//         BuyTicketComponent
//       ],
//       providers: [ { provide: Router, useValue: routerMock } ],
//       imports: [
//         AppMaterialModule
//       ]
//     });

//     fixture = TestBed.createComponent(BuyTicketComponent);
//     component = fixture.componentInstance;
//     router = TestBed.get(Router);
//   });

//   it('should navigate to purchased tickets page', () => {
//     component.showPurchasedTickets();
//     expect(router.navigate).toHaveBeenCalledWith(['tickets/purchased']);
//   });

// });