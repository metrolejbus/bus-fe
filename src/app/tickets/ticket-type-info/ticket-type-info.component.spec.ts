// import { TicketTypeInfoComponent } from "./ticket-type-info.component";
// import { ComponentFixture, TestBed, async } from "@angular/core/testing";
// import { Router, RouterModule } from "@angular/router";
// import { AppMaterialModule } from "src/app/app-material/app-material.module";
// import { BuyTicketComponent } from "../buy-ticket/buy-ticket.component";
// import { routes } from './../../app-routing.module';
// import { HomeComponent } from "src/app/modules/home/home.component";
// import { TicketComponent } from "../ticket.component";
// import { TicketTypesComponent } from "../ticket-types/ticket-types.component";
// import { PurchasedTicketsComponent } from "../purchased/purchased-tickets.component";
// import { ZoneService } from "src/app/common/services/zone.service";
// import { DebugElement } from '@angular/core';
// import { By } from '@angular/platform-browser';

// describe('TicketTypeInfoComponent', () => {
//   let component: TicketTypeInfoComponent;
//   let fixture: ComponentFixture<TicketTypeInfoComponent>;
//   let router: any;
//   let zoneService: any;

//   beforeEach(() => {
//     let zoneServiceMock = {
//       getStudents: jasmine.createSpy('getZones')
//           .and.returnValue(Promise.resolve([{}, {}, {}])),
//       RegenerateData$: {
//         subscribe: jasmine.createSpy('subscribe')
//       }
//     };

//     let routerMock = {
//       navigate: jasmine.createSpy('navigate')
//     };

//     TestBed.configureTestingModule({
//       declarations: [
//         TicketTypeInfoComponent,
//         HomeComponent,
//         TicketComponent,
//         TicketTypesComponent,
//         BuyTicketComponent,
//         PurchasedTicketsComponent
//       ],
//       providers: [
//         { provide: Router, useValue: routerMock },
//         { provide: ZoneService, useValue: zoneServiceMock }
//       ],
//       imports: [
//         AppMaterialModule,
//         RouterModule.forRoot(routes)
//       ]
//     });

//     fixture = TestBed.createComponent(TicketTypeInfoComponent);
//     component = fixture.componentInstance;
//     router = TestBed.get(Router);
//   });

//   it('should navigate to ticket purchasing page', () => {
//     let type: String = 'single';
//     component.onButtonBuy(type);
//     expect(router.navigate).toHaveBeenCalledWith([`/tickets/${type}/buy`]);
//   });

//   it('should fetch the list of zones on init', async(() => {
//     component.ngOnInit();

//     expect(zoneService.RegenerateData$.subscribe).toHaveBeenCalled();
//     expect(zoneService.getZones).toHaveBeenCalled();

//     fixture.whenStable()
//       .then(() => {
//         expect(component.zones.length).toBe(3);
//         fixture.detectChanges();
//         let elements: DebugElement[] = fixture.debugElement.queryAll(By.css('table tr'));
//         expect(elements.length).toBe(4);
//       });
//   }));

// });