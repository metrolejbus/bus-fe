import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ZoneService } from './../../zones/zone.service';
import { Zone } from 'src/app/zones/zone.model';
import { TicketPriceService } from '../services/ticket-price.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-ticket-type-info-component',
  templateUrl: './ticket-type-info.component.html',
  styleUrls: ['./ticket-type-info.component.css']
})
export class TicketTypeInfoComponent implements OnInit {
  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private zoneService: ZoneService,
    private router: Router,
    private ticketPriceService: TicketPriceService
  ) {
    const param = activatedRoute.snapshot.params['type'];
    this.type = param.substring(0, 1).toUpperCase() + param.substring(1);
  }

  zones: Zone[];

  customers = [
    { status: 'adult' },
    { status: 'student' },
    { status: 'senior' }
  ];
  messages = {
    student:
      'Students below the age of 30 and work experience trainees can get a 40% discount.',
    adult:
      'If you do not belong to a discounted group, you pay the adult price.',
    senior: 'The senior discount applies to everyone above the age of 67.'
  };
  selectedZone: string;
  type: string;
  typeOfPeriodic: string = "MONTH";
  ticketPrices;
  ticketPricesDict = {STUDENT: 'no price yet', ADULT: 'no price yet', SENIOR: 'no price yet'};
  disabled = true;
  price = '';
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSource;

  displayedColumns: string[] = [
    'customerStatus',
    'zone',
    'price'
  ];

  ngOnInit() {
    this.getZones();
    this.getTicketPrices();
  }

  getZones(): void {
    this.zoneService.getZones().subscribe(zones => (this.zones = zones));
  }

  onButtonBuy(type) {
    this.router.navigate([`/tickets/${type.toLowerCase()}/buy`]);
  }

  getTicketPrices() {
    var ticketType: String;
    switch (this.type) {
      case 'Single':
        ticketType = "SINGLE_USE";
        break;
      case 'Daily':
        ticketType = "DAY";
        break;
      case 'Periodic':
        if (this.typeOfPeriodic === 'Year') {
          ticketType = "YEAR";
          break;
        }
        ticketType = "MONTH";
    }
    this.ticketPriceService.getTicketPriceByDuration(ticketType)
      .subscribe(result => {
        this.dataSource = new MatTableDataSource<Object>(result);
        this.dataSource.paginator = this.paginator;
      });

  }

  private applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
