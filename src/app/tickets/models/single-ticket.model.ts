import {Zone} from '../../common/models/zone.model';
import { Journey } from '../../common/models/journey.model';

export class SingleTicket {
  public constructor(
    public id: number = null,
    public customer = null,
    public zone: Zone = null,
    public issuedOn = null,
    public active: boolean = null,
    public journey: Journey = null,
    public duration = null,
  ) {}
}
