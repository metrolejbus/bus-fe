export class TicketPrice {
  customerId;
  ticketDuration;
  zoneId;
  value;
  startDate;
  endDate;

  public constructor(
    customerId: string = null,
    ticketDuration: string = null,
    zoneId: string = null
  ) {
    this.customerId = customerId;
    this.ticketDuration = ticketDuration;
    this.zoneId = zoneId;
  }
}