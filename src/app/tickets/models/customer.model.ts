export class Customer {
  public id: number = null;
  public age: number = null;
  public lastName: string = null;
  public middleName: string = null;
  public firstName: string = null;
  public photoUrl: string = null;
  public status: string = null;
  public ticketVerificationFileName: string = null;

  public constructor(data) {
    Object.assign(this, data);
  }

  getFullName() {
    return `${this.firstName} ${this.lastName}`;
  }
}
