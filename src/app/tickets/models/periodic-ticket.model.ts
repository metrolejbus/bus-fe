import { Customer } from './customer.model';
import { Zone } from 'src/app/zones/zone.model';

export class PeriodicTicket {

  public constructor(
    public id: number = null,
    public active = false,
    public issuedOn: string = null,
    public duration: string = null,
    public rejectionReason: string = null,
    public state: string = null,
    public validFrom = null,
    public validTo: string = null,
    public customer: Customer = null,
    public validityPeriod: string = null,
    public zone: Zone = null
  ) {}

}
