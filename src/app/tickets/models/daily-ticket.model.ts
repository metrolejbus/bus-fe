import { Zone } from 'src/app/zones/zone.model';

export class DailyTicket {

  public constructor(
    public id: number = null,
    public customer = null,
    public zone: Zone = null,
    public issuedOn = null,
    public active: boolean = null,
    public date = null,
    public duration = null,
  ) {}
}
