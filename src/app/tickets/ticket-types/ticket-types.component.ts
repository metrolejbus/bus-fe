import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ticket-types-component',
  templateUrl: './ticket-types.component.html',
  styleUrls: [ './ticket-types.component.css' ]
})
export class TicketTypesComponent {
  constructor(private router: Router) {}
  ticketTypes: string[] = ['single', 'daily', 'periodic'];

  onTicketTypeClick(type) {
    this.router.navigate([`/tickets/${type.toLowerCase()}/info`]);
  }

}
