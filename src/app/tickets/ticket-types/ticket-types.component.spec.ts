import { TicketTypesComponent } from "./ticket-types.component";
import { ComponentFixture, TestBed } from "@angular/core/testing";
import { Router } from "@angular/router";
import { AppMaterialModule } from "src/app/app-material/app-material.module";

describe('TicketTypesComponent', () => {
  let component: TicketTypesComponent;
  let fixture: ComponentFixture<TicketTypesComponent>;
  let router: any;

  beforeEach(() => {
    let routerMock = {
      navigate: jasmine.createSpy('navigate')
    };

    TestBed.configureTestingModule({
      declarations: [ TicketTypesComponent ],
      providers: [ { provide: Router, useValue: routerMock } ],
      imports: [
        AppMaterialModule
      ]
    });

    fixture = TestBed.createComponent(TicketTypesComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
  });

  it('should navigate to ticket type info page', () => {
    let type: String = 'single';
    component.onTicketTypeClick(type);
    expect(router.navigate).toHaveBeenCalledWith([`/tickets/${type}/info`]);
  });

});