import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerService } from '../services/customer.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { SingleTicketService } from '../services/single-ticket.service';
import { SelectionModel } from '@angular/cdk/collections';
import { DailyTicketService } from '../services/daily-ticket.service';
import { PeriodicTicketService } from '../services/periodic-ticket.service';

@Component({
  selector: 'purchased-tickets-component',
  templateUrl: './purchased-tickets.component.html',
  styleUrls: [ './purchased-tickets.component.css' ]
})
export class PurchasedTicketsComponent {

  constructor(
    private customerService: CustomerService,
    private singleUseTicketService: SingleTicketService,
    private dailyTicketService: DailyTicketService,
    private periodicTicketService: PeriodicTicketService
  ) {}

  purchasedTickets;
  displayedColumnsSingle: string[] = ['select', 'Zone','Issued on'];
  displayedColumnsDaily: string[] = ['select', 'Zone', 'Date'];
  displayedColumnsPeriodic: string[] = ['select', 'Zone', 'Valid from', 'Duration'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  dataSourceSingle;
  dataSourceDaily;
  dataSourcePeriodic;

  private selectionSingleUse = new SelectionModel<Object>(true, []);
  private selectionDaily = new SelectionModel<Object>(true, []);
  private selectionPeriodic = new SelectionModel<Object>(true, []);

  ticket;
  id;

  ngOnInit() {
    this.loadTickets();
  }

  loadTickets() {
    this.customerService.getCustomersTickets()
      .subscribe(
        result => {
          this.dataSourceSingle = new MatTableDataSource<Object>(result['singleUseTickets']);
          this.dataSourceDaily = new MatTableDataSource<Object>(result['dailyTickets']);
          this.dataSourcePeriodic = new MatTableDataSource<Object>(result['periodicTickets']);
          this.dataSourceSingle.paginator = this.paginator;
          this.dataSourceDaily.paginator = this.paginator;
          this.dataSourcePeriodic.paginator = this.paginator;
        });
  }

  private select(element) {
    this.ticket = Object.assign(new Object(), element);
    this.id.setValue(element.id);
  }

  private useTickets(ticketType: string): void {
    switch (ticketType) {
      case 'single':
        this.useSingleTickets();
        break;
      case 'daily':
        this.useDailyTickets();
        break;
      case 'periodic':
        this.usePeriodicTickets();
        break;
    }
  }

  useSingleTickets() {
    this.selectionSingleUse.selected.forEach(el => {
      this.singleUseTicketService.delete(el['id']).subscribe(
        element => {
          const index = this.dataSourceSingle.data.findIndex(e => e.id === element.id);
          const data = this.dataSourceSingle.data;
          data.splice(index, 1);
          this.dataSourceSingle.data = data;
        },
        err => {
          console.log(err);
        }
      );
    });
  }

  useDailyTickets() {
    this.selectionDaily.selected.forEach(el => {
      this.dailyTicketService.delete(el['id']).subscribe(
        element => {
          const index = this.dataSourceDaily.data.findIndex(e => e.id === element.id);
          const data = this.dataSourceDaily.data;
          data.splice(index, 1);
          this.dataSourceDaily.data = data;
        },
        err => {
          console.log(err);
        }
      );
    });
  }

  usePeriodicTickets() {
    this.selectionPeriodic.selected.forEach(el => {
      this.periodicTicketService.delete(el['id']).subscribe(
        element => {
          const index = this.dataSourcePeriodic.data.findIndex(e => e.id === element.id);
          const data = this.dataSourcePeriodic.data;
          data.splice(index, 1);
          this.dataSourcePeriodic.data = data;
        },
        err => {
          console.log(err);
        }
      );
    });
  }

}
