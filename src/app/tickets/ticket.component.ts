import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tickets-component',
  templateUrl: './ticket.component.html',
  styleUrls: [ './ticket.component.css' ]
})
export class TicketComponent {

  constructor(private router: Router) {}

  onBuyOnlineButtonClick() {
    this.router.navigate(['/tickets/types']);
  }

  onSeeTicketsButtonClick() {
    this.router.navigate(['/tickets/purchased']);
  }
}
