import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TicketPrice } from '../models/ticket-price.model';

@Injectable({
  providedIn: 'root'
})
export class TicketPriceService {
  private ticketPricesUrl = '/ticket-prices';

  constructor(private http: HttpClient) {}

  create(status, duration, price, zone, startDate, endDate) {
    let startDateTokens = startDate.toLocaleDateString().split('/');
    let endDateTokens = endDate.toLocaleDateString().split('/');
    let newStartDate = `${startDateTokens[1]}.${startDateTokens[0]}.${startDateTokens[2]}.`;
    let newEndDate = `${endDateTokens[1]}.${endDateTokens[0]}.${endDateTokens[2]}.`;

    return this.http.post<TicketPrice> (
      this.ticketPricesUrl,
      {
        customerStatus: status,
        ticketDuration: duration,
        zoneId: zone,
        value: price,
        startDate: newStartDate,
        endDate: newEndDate
      }
    );
  }

  update(status, duration, price, zone, startDate, endDate) {
    var startDateTokens: string[];
    var endDateTokens: string[];
    var newEndDate;
    var newStartDate;
    if (startDate.indexOf('/') > -1) {
      startDateTokens = startDate.toLocaleDateString().split('/');
      endDateTokens = endDate.toLocaleDateString().split('/');
      newStartDate = `${startDateTokens[1]}.${startDateTokens[0]}.${startDateTokens[2]}.`;
      newEndDate = `${endDateTokens[1]}.${endDateTokens[0]}.${endDateTokens[2]}.`;
    } else {
      startDateTokens = startDate.split('-');
      endDateTokens = endDate.split('-');
      newStartDate = `${startDateTokens[2]}.${startDateTokens[1]}.${startDateTokens[0]}.`;
      newEndDate = `${endDateTokens[2]}.${endDateTokens[1]}.${endDateTokens[0]}.`;
    }

    return this.http.put<TicketPrice>(
      this.ticketPricesUrl,
      {
        customerStatus: status,
        ticketDuration: duration,
        zoneId: zone,
        value: price,
        startDate: newStartDate,
        endDate: newEndDate
      }
    )
  }

  loadData(): any {
    return this.http.get(this.ticketPricesUrl);
  }

  getTicketPrice(selectedZone, duration) {
    return this.http.get(`${this.ticketPricesUrl}/${selectedZone}/${duration}`);
  }

  getTicketPriceByDuration(duration): any {
    return this.http.get(`${this.ticketPricesUrl}/${duration}`);
  }

  delete(zone, duration, customer) {
    return this.http.delete<TicketPrice>(`${this.ticketPricesUrl}/${zone}/${duration}/${customer}`);
  }
}
