import { Zone } from "src/app/zones/zone.model";
import { PeriodicTicketService } from "./periodic-ticket.service";
import { PeriodicTicket } from "../models/periodic-ticket.model";
import { of } from "rxjs";

describe('PeriodicTicketService', () => {
  let periodicTicketService: PeriodicTicketService;
  let httpClient: {
    post: jasmine.Spy,
    delete: jasmine.Spy
  };

  beforeEach(() => {
    httpClient = jasmine.createSpyObj('HttpClient', ['post', 'delete']);
    periodicTicketService = new PeriodicTicketService(<any>httpClient);
  });

  it('createTicket() should query url and save a ticket', () => {
    let zone: Zone = new Zone({
      id: '1',
      name: 'I'
    });
    const expected = new PeriodicTicket();
    expected.id = 1;
    expected.zone = zone;
    expected.validFrom = '2/20/2019';
    expected.duration = 'MONTH';

    httpClient.post.and.returnValue(of(expected));
    periodicTicketService.createTicket(zone, '2/20/2019', 'MONTH')
      .subscribe(
        result => {
        expect(result).toBe(expected);
        },
        err => fail()
      );
      expect(httpClient.post.calls.count()).toBe(1);
  });

  it('delete() should query url and delete a ticket', () => {
    let zone: Zone = new Zone({
      id: '1',
      name: 'I'
    });
    const expected = new PeriodicTicket();
    expected.id = 1;
    expected.zone = zone;
    expected.validFrom = '2/20/2019';
    expected.duration = 'MONTH';

    httpClient.delete.and.returnValue(of(expected));

    periodicTicketService.delete(1).subscribe(
      result => {
        expect(result).toBe(expected);
      },
      err => fail()
    );
    expect(httpClient.delete.calls.count()).toBe(1);
  });

});