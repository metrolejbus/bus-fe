import { Zone } from "src/app/zones/zone.model";
import { of } from "rxjs";
import { TicketPriceService } from "./ticket-price.service";
import { TicketPrice } from "../models/ticket-price.model";

describe('TicketPriceService', () => {
  let ticketPriceService: TicketPriceService;

  let httpClient: {
    get: jasmine.Spy,
    put: jasmine.Spy,
    post: jasmine.Spy,
    delete: jasmine.Spy
  };

  beforeEach(() => {
    httpClient = jasmine.createSpyObj('HttpClient', ['post', 'delete', 'get', 'put']);
    ticketPriceService = new TicketPriceService(<any>httpClient);
  });

  it('create() should query url and save a ticket price', () => {
    const expected = new TicketPrice();
    expected.zoneId = 1;
    expected.ticketDuration = 'MONTH';
    expected.customerId = 0;
    httpClient.post.and.returnValue(of(expected));
    let startDate = new Date();
    startDate.setFullYear(2019, 2, 20);
    let endDate = new Date();
    endDate.setFullYear(2019, 2, 25);
    ticketPriceService.create('STUDENT', 'MONTH', 20, 1, startDate, endDate)
      .subscribe(
        result => {
        expect(result).toBe(expected);
      },
        err => fail()
      );

    expect(httpClient.post.calls.count()).toBe(1);
  });

  it('delete() should query url and delete a ticket price', () => {
    const expected = new TicketPrice();
    expected.zoneId = 1;
    expected.ticketDuration = 'MONTH';
    expected.customerId = 0;
    httpClient.delete.and.returnValue(of(expected));

    ticketPriceService.delete(1, 'MONTH', 'STUDENT').subscribe(
      result => {
        expect(result).toBe(expected);
      },
      err => fail()
    );

    expect(httpClient.delete.calls.count()).toBe(1);
  });

  it('loadData() should get all ticket prices', () => {
    const expected1 = new TicketPrice();
    expected1.zoneId = 1;
    expected1.ticketDuration = 'MONTH';
    expected1.customerId = 0;

    const expected2 = new TicketPrice();
    expected2.zoneId = 1;
    expected2.ticketDuration = 'YEAR';
    expected2.customerId = 0;
    const expectedTicketPrices = [expected1, expected2];
    httpClient.get.and.returnValue(of(expectedTicketPrices));

    ticketPriceService.loadData().subscribe(
      result => {
        expect(result).toBe(expectedTicketPrices);
      },
      err => fail()
    );

    expect(httpClient.get.calls.count()).toBe(1);
  });

  it('getTicketPrice() should get ticket prices by zone and duration', () => {
    const expected = new TicketPrice();
    expected.zoneId = 1;
    expected.ticketDuration = 'MONTH';
    expected.customerId = 0;
    httpClient.get.and.returnValue(of(expected));

    ticketPriceService.getTicketPrice(1, 'MONTH').subscribe(
      result => {
        expect(result).toBe(expected);
      },
      err => fail()
    );

    expect(httpClient.get.calls.count()).toBe(1);
  });

  it('getTicketPriceByDuration() should get ticket prices by duration', () => {
    const expected = new TicketPrice();
    expected.zoneId = 1;
    expected.ticketDuration = 'MONTH';
    expected.customerId = 0;
    httpClient.get.and.returnValue(of(expected));

    ticketPriceService.getTicketPriceByDuration('MONTH').subscribe(
      result => {
        expect(result).toBe(expected);
      },
      err => fail()
    );

    expect(httpClient.get.calls.count()).toBe(1);
  });

});