import { Zone } from "src/app/zones/zone.model";
import { SingleTicketService } from "./single-ticket.service";
import { SingleTicket } from "../models/single-ticket.model";
import { of } from "rxjs";

describe('SingleTicketService', () => {
  let singleTicketService: SingleTicketService;
  let httpClient: {
    post: jasmine.Spy,
    delete: jasmine.Spy
  };

  beforeEach(() => {
    httpClient = jasmine.createSpyObj('HttpClient', ['post', 'delete']);
    singleTicketService = new SingleTicketService(<any>httpClient);
  });

  it('createTicket() should query url and save a ticket', () => {
    let zone: Zone = new Zone({
      id: '1',
      name: 'I'
    });
    const expected = new SingleTicket();
    expected.id = 1;
    expected.zone = zone;

    httpClient.post.and.returnValue(of(expected));
    singleTicketService.createTicket(zone)
      .subscribe(
        result => {
        expect(result).toBe(expected);
      },
        err => fail()
      );

    expect(httpClient.post.calls.count()).toBe(1);
  });

  it('delete() should query url and delete a ticket', () => {
    let zone: Zone = new Zone({
      id: '1',
      name: 'I'
    });
    const expected = new SingleTicket();
    expected.id = 1;
    expected.zone = zone;

    httpClient.delete.and.returnValue(of(expected));
    singleTicketService.delete(1).subscribe(
      result => {
        expect(result).toBe(expected);
      },
      err => fail()
    );

    expect(httpClient.delete.calls.count()).toBe(1);
  });

});