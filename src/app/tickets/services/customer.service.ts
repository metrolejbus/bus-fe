import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DailyTicket } from './../models/daily-ticket.model';
import { Zone } from 'src/app/zones/zone.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {
  private customerUrl = '/customers';

  constructor(private http: HttpClient) {}

  getCustomersTickets() {
    return this.http.get(this.customerUrl + '/tickets');
  }
}
