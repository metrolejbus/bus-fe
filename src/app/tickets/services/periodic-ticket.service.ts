import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { PeriodicTicket } from './../models/periodic-ticket.model';
import { Zone } from 'src/app/zones/zone.model';

@Injectable({
  providedIn: 'root'
})
export class PeriodicTicketService {
  private periodicTicketsUrl = '/periodic-tickets';

  constructor(private http: HttpClient) {}

  createTicket(zone: Zone, validFrom, duration: string): Observable<PeriodicTicket> {
    return this.http.post<PeriodicTicket>(this.periodicTicketsUrl, {
      zoneId: zone.id,
      validFrom,
      duration
    });
  }

  getUnprocessedTickets(): Promise<Array<object>> {
    return this.http.get<Array<object>>(`${this.periodicTicketsUrl}/unprocessed`).toPromise();
  }

  acceptTicketRequest(ticket: PeriodicTicket ) {
    return this.http.post(`${this.periodicTicketsUrl}/${ticket.id}/approve`, {}).toPromise();
  }

  rejectTicketRequest(ticket: PeriodicTicket ) {
    return this.http.post(`${this.periodicTicketsUrl}/${ticket.id}/reject`, {}).toPromise();
  }

  delete(id) {
    return this.http.delete<PeriodicTicket>(`${this.periodicTicketsUrl}/${id}`);
  }
}
