import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { SingleTicket } from './../models/single-ticket.model';
import { Zone } from 'src/app/zones/zone.model';

@Injectable({
  providedIn: 'root'
})
export class SingleTicketService {
  private singleTicketsUrl = '/single-use-tickets';

  constructor(private http: HttpClient) {}

  createTicket(zone: Zone): Observable<SingleTicket> {
    return this.http.post<SingleTicket>(this.singleTicketsUrl, {
      zoneId: zone.id
    });
  }

  delete(id) {
    return this.http.delete<SingleTicket>(`${this.singleTicketsUrl}/${id}`);
  }
}
