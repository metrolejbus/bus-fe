import { DailyTicketService } from "./daily-ticket.service";
import { Zone } from "src/app/zones/zone.model";
import { of } from "rxjs";
import { DailyTicket } from "../models/daily-ticket.model";

describe('DailyTicketService', () => {
  let dailyTicketService: DailyTicketService;

  let httpClient: {
    post: jasmine.Spy,
    delete: jasmine.Spy
  };

  beforeEach(() => {
    httpClient = jasmine.createSpyObj('HttpClient', ['post', 'delete']);
    dailyTicketService = new DailyTicketService(<any>httpClient);
  });

  it('createTicket() should query url and save a ticket', () => {
    let zone: Zone = new Zone({
      id: '1',
      name: 'I'
    });
    const expected = new DailyTicket();
    expected.id = 1;
    expected.zone = zone;
    expected.date = '2/20/2019';
    httpClient.post.and.returnValue(of(expected));
    dailyTicketService.createTicket(zone, '2/20/2019')
      .subscribe(
        result => {
        expect(result).toBe(expected);
      },
        err => fail()
      );

    expect(httpClient.post.calls.count()).toBe(1);
  });

  it('delete() should query url and delete a ticket', () => {
    let zone: Zone = new Zone({
      id: '1',
      name: 'I'
    });
    const expected = new DailyTicket();
    expected.id = 1;
    expected.zone = zone;
    expected.date = '2/20/2019';
    httpClient.delete.and.returnValue(of(expected));

    dailyTicketService.delete(1).subscribe(
      result => {
        expect(result).toBe(expected);
      },
      err => fail()
    );

    expect(httpClient.delete.calls.count()).toBe(1);
  });

});