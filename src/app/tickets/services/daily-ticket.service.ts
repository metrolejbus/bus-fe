import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DailyTicket } from './../models/daily-ticket.model';
import { Zone } from 'src/app/zones/zone.model';

@Injectable({
  providedIn: 'root'
})
export class DailyTicketService {
  private dailyTicketsUrl = '/daily-tickets';

  constructor(private http: HttpClient) {}

  createTicket(zone: Zone, date: string): Observable<DailyTicket> {
    return this.http.post<DailyTicket>(this.dailyTicketsUrl, {
      zoneId: zone.id,
      date: date
    });
  }

  delete(id) {
    return this.http.delete<DailyTicket>(`${this.dailyTicketsUrl}/${id}`);
  }
}
