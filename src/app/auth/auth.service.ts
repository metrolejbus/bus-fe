import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

const ENDPOINTS = {
  LOGIN: '/auth/sign-in',
  REGISTER: '/auth/sign-up'
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(private http: HttpClient) {}

  login(username, password): Promise<object> {
    return this.http
      .post(ENDPOINTS.LOGIN, {
        username,
        password
      })
      .toPromise()
      .then(response => {
        this.startSession(response);
        return response;
      });
  }

  register(data): Promise<object> {
    return this.http
      .post(ENDPOINTS.REGISTER, data)
      .toPromise()
      .then(response => {
        this.startSession(response);
        return response;
      });
  }

  logout(): void {
    // api...
    localStorage.clear();
  }

  startSession(data): void {
    localStorage.setItem('user', JSON.stringify(data.user));
    localStorage.setItem('token', JSON.stringify(data.token));
  }

  isAuthenticated(): boolean {
    return !!localStorage.getItem('token');
  }

  getUser() {
    return JSON.parse(localStorage.getItem('user'));
  }

  isAdmin(): boolean {
    return this.isAuthenticated() && this.getUser().role === 'ADMIN';
  }

  isCustomer(): boolean {
    return this.isAuthenticated() && this.getUser().role === 'CUSTOMER';
  }

  isOperator(): boolean {
    return this.isAuthenticated() && this.getUser().role === 'OPERATOR';
  }

  isSystem(): boolean {
    return this.isAuthenticated() && this.getUser().role === 'SYSTEM';
  }
}
