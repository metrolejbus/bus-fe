import { RegisterComponent } from './register/register.component';
import { AppMaterialModule } from './../app-material/app-material.module';
import { LoginComponent } from './login/login.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ModuleRouting } from './auth-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [RegisterComponent, LoginComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    ModuleRouting,
    CommonModule,
    HttpClientModule,
    AppMaterialModule
  ]
})
export class AuthModule {}
