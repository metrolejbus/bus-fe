import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) {}

  username = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required]);
  errorMessage = false;

  ngOnInit() {}

  submitLogin() {
    if (!!this.getUsernameErrorMessage() || !!this.getPasswordErrorMessage()) {
      return;
    }
    this.authService
      .login(this.username.value, this.password.value)
      .then(response => {
        this.router.navigate(['/']);
      })
      .catch(err => {
        this.errorMessage = true;
      });
  }

  getUsernameErrorMessage() {
    return this.username.hasError('required')
      ? 'Username can not be empty'
      : '';
  }

  getPasswordErrorMessage() {
    return this.password.hasError('required')
      ? 'Password can not be empty'
      : '';
  }
}
