import { TestBed, async, inject } from '@angular/core/testing';

import { NoauthGuard } from './noauth.guard';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('NoauthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NoauthGuard],
      imports: [
        HttpClientModule,
        RouterTestingModule
      ]
    });
  });

  it('should ...', inject([NoauthGuard], (guard: NoauthGuard) => {
    expect(guard).toBeTruthy();
  }));
});
