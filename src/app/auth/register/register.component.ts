import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) {}

  STATUSES = [
    'STUDENT', 'ADULT', 'SENIOR'
  ];

  email = new FormControl('', [Validators.required, Validators.email]);
  username = new FormControl('', [Validators.required]);
  password = new FormControl('', [Validators.required]);
  firstName = new FormControl('', [Validators.required]);
  lastName = new FormControl('', [Validators.required]);
  middleName = new FormControl('', []);
  status = new FormControl('', [Validators.required]);
  age = new FormControl(0, [Validators.required, Validators.min(1)]);

  ngOnInit() {}
  submitRegister() {
    if (!!this.getEmailErrorMessage() || !!this.getPasswordErrorMessage()) {
      return;
    }
    this.authService
      .register({
        email: this.email.value,
        password: this.password.value,
        username: this.username.value,
        firstName: this.firstName.value,
        lastName: this.lastName.value,
        middleName: this.middleName.value,
        status: this.status,
        age: this.age.value
      })
      .then(response => {
        this.router.navigate(['/']);
      });
  }

  getEmailErrorMessage() {
    return this.email.hasError('required')
      ? 'Email can not be empty'
      : this.email.hasError('email')
      ? 'Not a valid email'
      : '';
  }

  getPasswordErrorMessage() {
    return this.password.hasError('required')
      ? 'Password can not be empty'
      : '';
  }
}
