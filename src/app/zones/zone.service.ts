import { Injectable } from '@angular/core';
import { Zone } from './zone.model';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ZoneService {

  private zonesUrl = '/zones';

  constructor(private http: HttpClient) { }

  getZones(): Observable<Zone[]> {
    return this.http.get<Zone[]>(this.zonesUrl);
  }

}
