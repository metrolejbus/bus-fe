export class Zone {
  public id: number;
  public name: string;

  public constructor(data = {}) {
    Object.assign(this, data);
  }
}
