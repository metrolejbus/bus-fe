import { environment } from './../environments/environment';
import { ApiHttpInterceptor } from './api-http-interceptor';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppMaterialModule } from './app-material/app-material.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeModule } from 'src/app/modules/home/home.module';
import { ProfileModule } from './modules/profile/profile.module';
import { TicketComponent } from './tickets/ticket.component';
import { TicketTypesComponent } from './tickets/ticket-types/ticket-types.component';
import { TicketTypeInfoComponent } from './tickets/ticket-type-info/ticket-type-info.component';
import { AdminModule } from 'src/app/modules/admin/admin.module';
import { PurchasedTicketsComponent } from './tickets/purchased/purchased-tickets.component';
import { BuyTicketComponent, ConfirmPurchaseDialog } from './tickets/buy-ticket/buy-ticket.component';
import { ConfirmDeleteDialog } from './modules/admin/components/ticket-price-page/ticket-price-page.component';

@NgModule({
  declarations: [
    AppComponent,
    TicketComponent,
    TicketTypesComponent,
    TicketTypeInfoComponent,
    BuyTicketComponent,
    PurchasedTicketsComponent,
    ConfirmPurchaseDialog,
    ConfirmDeleteDialog
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,

    AppMaterialModule,
    LeafletModule,

    HomeModule,
    ProfileModule,

    AppRoutingModule,
  ],
  providers: [
    { provide: 'BASE_API_URL', useValue: environment.baseApiUrl },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiHttpInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ConfirmPurchaseDialog,
    ConfirmDeleteDialog
  ],
})
export class AppModule {}
