import { HomeComponent } from './modules/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TicketComponent } from './tickets/ticket.component';
import { TicketTypesComponent } from './tickets/ticket-types/ticket-types.component';
import { TicketTypeInfoComponent } from './tickets/ticket-type-info/ticket-type-info.component';
import { BuyTicketComponent } from './tickets/buy-ticket/buy-ticket.component';
import { PurchasedTicketsComponent } from './tickets/purchased/purchased-tickets.component';

export const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'tickets', component: TicketComponent },
  { path: 'tickets/types', component: TicketTypesComponent },
  { path: 'tickets/:type/info', component: TicketTypeInfoComponent },
  { path: 'tickets/:type/buy', component: BuyTicketComponent},
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
  { path: 'admin', loadChildren: './modules/admin/admin.module#AdminModule' },
  { path: 'profile', loadChildren: './modules/profile/profile.module#ProfileModule' },
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'tickets/:type/buy', component: BuyTicketComponent },
  { path: 'tickets/purchased', component: PurchasedTicketsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

