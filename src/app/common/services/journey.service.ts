import { Injectable } from '@angular/core';
import { SocketService } from './socket.service';
import { Observable } from 'rxjs';
import { TimedLocation } from '../models/timed-location.model';


@Injectable({
  providedIn: 'root',
})
export class JourneyService {

  public locations: Observable<TimedLocation>;
  public journeyEnds: Observable<number>;

  public constructor(public socketService: SocketService) {
    this.locations = new Observable(ob => {
      this.socketService.on('journey-location', (location: TimedLocation) => {
        ob.next(location);
      });
    });

    this.journeyEnds = new Observable(ob => {
      this.socketService.on('journey-end', (journeyId: number) => {
        ob.next(journeyId);
      });
    });
  }
}
