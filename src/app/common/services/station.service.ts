import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Station } from 'src/app/common/models/station.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class StationService {

  private URL = '/stations';

  public constructor(private http: HttpClient) {
  }

  public createStation(name: string, zoneId: number, lat: number, lng: number): Observable<Station> {
    const req = { name, zoneId, lat, lng };
    return this.http.post<Station>(this.URL, req);
  }

  public deleteStation(id: number): Observable<Station> {
    return this.http.delete<Station>(`${this.URL}/${id}`);
  }

  public getStation(id: number): Observable<Station> {
    return this.http.get<Station>(`${this.URL}/${id}`);
  }

  public getStations(): Observable<Station[]> {
    return this.http.get<Station[]>(this.URL);
  }

  public updateStation(id: number, name: string, zoneId: number, lat: number, lng: number): Observable<Station> {
    const req = { name, zoneId, lat, lng };
    return this.http.put<Station>(`${this.URL}/${id}`, req);
  }
}
