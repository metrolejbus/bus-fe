import { ZoneService } from './zone.service';
import { Zone } from '../models/zone.model';
import {
  of,
  throwError
} from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';


describe('ZoneServiceTest', () => {
  let httpClient: {
    get: jasmine.Spy,
    post: jasmine.Spy,
    delete: jasmine.Spy,
    put: jasmine.Spy
  };
  let service: ZoneService;

  beforeEach(() => {
    httpClient = jasmine.createSpyObj('HttpClient', ['post', 'get', 'put', 'delete']);
    service = new ZoneService(<any>httpClient);
  });

  it('create - success', () => {
    const expectedZone = new Zone(1, 'Zone 1');
    httpClient.post.and.returnValue(of(expectedZone));

    service.createZone('Zone 1').subscribe(
      zone => {
        expect(zone).toBe(expectedZone);
      },
      err => fail()
    );

    expect(httpClient.post.calls.count()).toBe(1);
  });

  it('create - name not unique', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Name not unique',
      status: 400, statusText: 'Bad Request'
    });

    httpClient.post.and.returnValue(throwError(errorResponse));

    service.createZone('Zone 1').subscribe(
      zone => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );
  });

  it('create - name is null', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Name must be provided',
      status: 400, statusText: 'Bad Request'
    });

    httpClient.post.and.returnValue(throwError(errorResponse));

    service.createZone('Zone 1').subscribe(
      zone => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );
  });

  it('get - success', () => {
    const expectedZone = new Zone(1, 'Zone 1');
    httpClient.get.and.returnValue(of(expectedZone));

    service.getZone(1).subscribe(
      zone => {
        expect(zone).toBe(expectedZone);
      },
      err => fail()
    );

    expect(httpClient.get.calls.count()).toBe(1);
  });

  it('get - not found', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Zone not found',
      status: 404, statusText: 'Not Found'
    });

    httpClient.get.and.returnValue(throwError(errorResponse));

    service.getZone(99).subscribe(
      zone => fail(),
      err => expect(err.message).toContain('404 Not Found')
    );
  });

  it('get all - success', () => {
    const expectedZones = [new Zone(1, 'Zone 1'), new Zone(2, 'Zone2')];
    httpClient.get.and.returnValue(of(expectedZones));

    service.getZones().subscribe(
      zones => {
        expect(zones).toBe(expectedZones);
      },
      err => fail()
    );

    expect(httpClient.get.calls.count()).toBe(1);
  });

  it('delete - success', () => {
    const expectedZone = new Zone(1, 'Zone 1');
    httpClient.delete.and.returnValue(of(expectedZone));

    service.deleteZone(1).subscribe(
      zone => {
        expect(zone).toBe(expectedZone);
      },
      err => fail()
    );

    expect(httpClient.delete.calls.count()).toBe(1);
  });

  it('delete - not found', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Zone not found',
      status: 404, statusText: 'Not Found'
    });

    httpClient.delete.and.returnValue(throwError(errorResponse));

    service.deleteZone(99).subscribe(
      zone => fail(),
      err => expect(err.message).toContain('404 Not Found')
    );
  });

  it('update - success', () => {
    const expectedZone = new Zone(1, 'Zone 2');
    httpClient.put.and.returnValue(of(expectedZone));

    service.updateZone(1, 'Zone 2').subscribe(
      zone => {
        expect(zone).toBe(expectedZone);
      },
      err => fail()
    );

    expect(httpClient.put.calls.count()).toBe(1);
  });

  it('update - not found', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Zone not found',
      status: 404, statusText: 'Not Found'
    });

    httpClient.put.and.returnValue(throwError(errorResponse));

    service.updateZone(99, 'Zone 1').subscribe(
      zone => fail(),
      err => expect(err.message).toContain('404 Not Found')
    );
  });

  it('update - bad request', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Name not unique',
      status: 404, statusText: 'Bad Request'
    });

    httpClient.put.and.returnValue(throwError(errorResponse));

    service.updateZone(1, 'Zone 1').subscribe(
      zone => fail(),
      err => expect(err.message).toContain('404 Bad Request')
    );
  });

});
