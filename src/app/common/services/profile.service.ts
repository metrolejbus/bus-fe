import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


const ENDPOINTS = {
  VERIFY: '/customers/personal-verification',
  FETCH: '/customers/',
};

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  public constructor(private http: HttpClient) {}

  uploadProfileVerificationInfo(fileToUpload): Promise<object> {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);

    return this.http.post(ENDPOINTS.VERIFY, formData).toPromise();
  }

  fetchProfileInfo() {
    const user = JSON.parse(localStorage.getItem('user'));
    return this.http.get(ENDPOINTS.FETCH + user.id).toPromise().then(data => {
      localStorage.setItem('user', JSON.stringify({...user, customer: data}));
      return data;
    });
  }
}
