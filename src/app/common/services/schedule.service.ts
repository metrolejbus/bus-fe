import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Schedule } from '../../models/schedule.model';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root',
})
export class ScheduleService {
  private URL = '/schedules';

  public constructor(private http: HttpClient) {

  }

  public createSchedule(
    routeA: string[], routeB: string[], days: DaysOfWeek[],startDate: string, endDate: string, line: number
  ): Observable<Schedule> {
    return this.http.post<Schedule>(this.URL, {routeA, routeB, days, startDate,endDate, line });
  }

  public deleteSchedule(id: number): Observable<Schedule> {
    return this.http.delete<Schedule>(`${this.URL}/${id}`);
  }

  public getSchedule(id: number): Observable<Schedule> {
    return this.http.get<Schedule>(`${this.URL}/${id}`);
  }

  public getSchedules(): Observable<Schedule[]> {
    return this.http.get<Schedule[]>(this.URL);
  }

  public updateSchedule(
    id: number, name: String, route: String
  ): Observable<Schedule> {
    return this.http.put<Schedule>(`${this.URL}/${id}`, { name, route});
  }
}
