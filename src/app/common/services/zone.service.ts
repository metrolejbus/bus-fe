import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Zone } from 'src/app/common/models/zone.model';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root',
})
export class ZoneService {

  private URL = '/zones';

  public constructor(private http: HttpClient) {
  }

  public createZone(name: string): Observable<Zone> {
    return this.http.post<Zone>(this.URL, { name });
  }

  public deleteZone(id: number): Observable<Zone> {
    return this.http.delete<Zone>(`${this.URL}/${id}`);
  }

  public getZone(id: number): Observable<Zone> {
    return this.http.get<Zone>(`${this.URL}/${id}`);
  }

  public getZones(): Observable<Zone[]> {
    return this.http.get<Zone[]>(this.URL);
  }

  public updateZone(id: number, name: string): Observable<Zone> {
    return this.http.put<Zone>(`${this.URL}/${id}`, { name });
  }
}
