import { HttpErrorResponse } from '@angular/common/http';
import {
  of,
  throwError
} from 'rxjs';

import { StationService } from './station.service';
import { Station } from '../models/station.model';
import { Zone } from '../models/zone.model';


describe('StationServiceTest', () => {
  let httpClient: {
    get: jasmine.Spy,
    post: jasmine.Spy,
    delete: jasmine.Spy,
    put: jasmine.Spy
  };
  let service: StationService;
  let zone: Zone;

  beforeEach(() => {
    zone = new Zone(1, 'Zone 1');
    httpClient = jasmine.createSpyObj('HttpClient', ['post', 'get', 'put', 'delete']);
    service = new StationService(<any>httpClient);
  });

  it('create - success', () => {
    const expectedStation = new Station(1, 'Station 1', zone, { lat: 30, lng: 30 });
    httpClient.post.and.returnValue(of(expectedStation));

    service.createStation('Station 1', 1, 30, 30).subscribe(
      station => {
        expect(station).toBe(expectedStation);
      },
      err => fail()
    );

    expect(httpClient.post.calls.count()).toBe(1);
  });

  it('create - zone not found', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Zone not found',
      status: 404, statusText: 'Not Found'
    });

    httpClient.post.and.returnValue(throwError(errorResponse));

    service.createStation('Station 1', 99, 30, 30).subscribe(
      station => fail(),
      err => expect(err.message).toContain('404 Not Found')
    );
  });

  it('create - name is null', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Name must be provided',
      status: 400, statusText: 'Bad Request'
    });

    httpClient.post.and.returnValue(throwError(errorResponse));

    service.createStation('Station 1', 1, 30, 30).subscribe(
      station => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );
  });

  it('create - lat not valid', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Latitude not valid',
      status: 400, statusText: 'Bad Request'
    });

    httpClient.post.and.returnValue(throwError(errorResponse));

    service.createStation('Station 1', 1, -99, 30).subscribe(
      station => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );

    service.createStation('Station 1', 1, 99, 30).subscribe(
      station => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );
  });

  it('create - lng not valid', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Longitude not valid',
      status: 400, statusText: 'Bad Request'
    });

    httpClient.post.and.returnValue(throwError(errorResponse));

    service.createStation('Station 1', 1, 30, -199).subscribe(
      station => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );

    service.createStation('Station 1', 1, 30, 199).subscribe(
      station => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );
  });

  it('get - success', () => {
    const expectedStation = new Station(1, 'Station 1', zone, { lat: 30, lng: 30 });
    httpClient.get.and.returnValue(of(expectedStation));

    service.getStation(1).subscribe(
      station => {
        expect(station).toBe(expectedStation);
      },
      err => fail()
    );

    expect(httpClient.get.calls.count()).toBe(1);
  });

  it('get - not found', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Station not found',
      status: 404, statusText: 'Not Found'
    });

    httpClient.get.and.returnValue(throwError(errorResponse));

    service.getStation(99).subscribe(
      station => fail(),
      err => expect(err.message).toContain('404 Not Found')
    );
  });

  it('get all - success', () => {
    const expectedStations = [
      new Station(1, 'Station 1', zone, { lat: 30, lng: 30 }),
      new Station(2, 'Station 2', zone, { lat: 36, lng: 36 })
    ];
    httpClient.get.and.returnValue(of(expectedStations));

    service.getStations().subscribe(
      stations => {
        expect(stations).toBe(expectedStations);
      },
      err => fail()
    );

    expect(httpClient.get.calls.count()).toBe(1);
  });

  it('delete - success', () => {
    const expectedStation = new Station(1, 'Station 1', zone, { lat: 30, lng: 30 });
    httpClient.delete.and.returnValue(of(expectedStation));

    service.deleteStation(1).subscribe(
      station => {
        expect(station).toBe(expectedStation);
      },
      err => fail()
    );

    expect(httpClient.delete.calls.count()).toBe(1);
  });

  it('delete - not found', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Station not found',
      status: 404, statusText: 'Not Found'
    });

    httpClient.delete.and.returnValue(throwError(errorResponse));

    service.deleteStation(99).subscribe(
      station => fail(),
      err => expect(err.message).toContain('404 Not Found')
    );
  });

  it('update - success', () => {
    const expectedStation = new Station(1, 'Station 99', zone, { lat: 36, lng: 36 });
    httpClient.put.and.returnValue(of(expectedStation));

    service.updateStation(1, 'Station 99', 1, 36, 36).subscribe(
      station => {
        expect(station).toBe(expectedStation);
      },
      err => fail()
    );

    expect(httpClient.put.calls.count()).toBe(1);
  });

  it('update - not found', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Station not found',
      status: 404, statusText: 'Not Found'
    });

    httpClient.put.and.returnValue(throwError(errorResponse));

    service.updateStation(99, 'Station 99', 1, 36, 36).subscribe(
      station => fail(),
      err => expect(err.message).toContain('404 Not Found')
    );
  });

  it('update - zone not found', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Zone not found',
      status: 404, statusText: 'Not Found'
    });

    httpClient.put.and.returnValue(throwError(errorResponse));

    service.updateStation(1, 'Station 99', 99, 36, 36).subscribe(
      station => fail(),
      err => expect(err.message).toContain('404 Not Found')
    );
  });

  it('update - bad request', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Name not unique',
      status: 404, statusText: 'Bad Request'
    });

    httpClient.put.and.returnValue(throwError(errorResponse));

    service.updateStation(1, 'Station 99', 1, 36, 36).subscribe(
      station => fail(),
      err => expect(err.message).toContain('404 Bad Request')
    );
  });

  it('update - lat not valid', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Latitude not valid',
      status: 400, statusText: 'Bad Request'
    });

    httpClient.put.and.returnValue(throwError(errorResponse));

    service.updateStation(1, 'Station 1', 1, -99, 30).subscribe(
      station => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );

    service.updateStation(1, 'Station 1', 1, 99, 30).subscribe(
      station => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );
  });

  it('update - lng not valid', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Longitude not valid',
      status: 400, statusText: 'Bad Request'
    });

    httpClient.put.and.returnValue(throwError(errorResponse));

    service.updateStation(1, 'Station 1', 1, 30, -199).subscribe(
      station => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );

    service.updateStation(1, 'Station 1', 1, 30, 199).subscribe(
      station => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );
  });

});
