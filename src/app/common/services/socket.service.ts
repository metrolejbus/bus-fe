import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';

@Injectable({
  providedIn: 'root',
})
export class SocketService {
  private socket: SocketIOClient.Socket;

  public constructor() {
    this.socket = io('http://localhost:9292');
  }

  public on(event, callback) {
    this.socket.on(event, callback);
  }
}
