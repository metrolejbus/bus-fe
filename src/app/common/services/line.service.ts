import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Line } from '../models/line.model';
import { Injectable } from '@angular/core';
import { Schedule } from 'src/app/models/schedule.model';

@Injectable({
  providedIn: 'root',
})
export class LineService {
  private URL = '/lines';

  public constructor(private http: HttpClient) {

  }

  public static routeToString(
    route: { lat: number, lng: number, station: boolean }[]
  ): String {
    const converted = [];
    route.forEach(point => {
      converted.push(`${point.lat},${point.lng},${point.station ? 1 : 0}`);
    });
    return converted.join(';');
  }

  public static routeFromString(
    route: String
  ): { lat: number, lng: number, station: boolean}[] {
    const converted = route.split(';');
    const points: { lat: number, lng: number, station: boolean}[] = [];
    let tokens: String[];
    converted.forEach(point => {
      tokens = point.split(',');
      points.push({
        lat: Number(tokens[0]),
        lng: Number(tokens[1]),
        station: Boolean(tokens[2])
      });
    });
    return points;
  }

  public createLine(name: String, type: String, route: String): Observable<Line> {
    return this.http.post<Line>(this.URL, { name, type, route });
  }

  public deleteLine(id: number): Observable<Line> {
    return this.http.delete<Line>(`${this.URL}/${id}`);
  }

  public getLine(id: number): Observable<Line> {
    return this.http.get<Line>(`${this.URL}/${id}`);
  }

  public getLines(): Observable<Line[]> {
    return this.http.get<Line[]>(this.URL);
  }

  public updateLine(id: number, name: String, route: String): Observable<Line> {
    return this.http.put<Line>(`${this.URL}/${id}`, { name, route});
  }

  public addStation(id: number, stationId: number): Observable<Line> {
    return this.http.post<Line>(`${this.URL}/${id}/stations`, { stationId });
  }

  public addStations(id: number, stationIds: number[]): Observable<Line> {
    return this.http.post<Line>(
      `${this.URL}/${id}/stations/bulk`, { stationIds }
    );
  }

  public removeStation(id: number, stationId: number): Observable<Line> {
    return this.http.request<Line>(
      'DELETE', `${this.URL}/${id}/stations`, { body: { stationId }}
    );
  }

  public removeStations(id: number, stationIds: number[]): Observable<Line> {
    return this.http.request<Line>(
      'DELETE', `${this.URL}/${id}/stations`, { body: { stationIds }}
    );
  }

  public getSchedules(lineId: number): Observable<Schedule[]> {
    return this.http.get<Schedule[]>(`${this.URL}/${lineId}/schedules`);
  }

}
