import {
  of,
  throwError
} from 'rxjs';

import { LineService } from './line.service';
import { Line } from '../models/line.model';
import { HttpErrorResponse } from '@angular/common/http';


describe('LineServiceTest', () => {
  let httpClient: {
    get: jasmine.Spy,
    post: jasmine.Spy,
    delete: jasmine.Spy,
    put: jasmine.Spy
  };
  let service: LineService;

  beforeEach(() => {
    httpClient = jasmine.createSpyObj('HttpClient', ['post', 'get', 'put', 'delete']);
    service = new LineService(<any>httpClient);
  });

  it('create - success', () => {
    const expectedLine = new Line(1, 'Line 1', 'BUS', [], [], 'route');
    httpClient.post.and.returnValue(of(expectedLine));

    service.createLine('Line 1', 'BUS', 'route').subscribe(
      line => {
        expect(line).toBe(expectedLine);
      },
      err => fail()
    );

    expect(httpClient.post.calls.count()).toBe(1);
  });

  it('create - name is null', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Name must be provided',
      status: 400, statusText: 'Bad Request'
    });

    httpClient.post.and.returnValue(throwError(errorResponse));

    service.createLine(null, 'BUS', 'route').subscribe(
      line => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );
  });

  it('create - type is null', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Type must be provided',
      status: 400, statusText: 'Bad Request'
    });

    httpClient.post.and.returnValue(throwError(errorResponse));

    service.createLine('Line 1', null, 'route').subscribe(
      line => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );
  });

  it('create - route is null', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Route must be provided',
      status: 400, statusText: 'Bad Request'
    });

    httpClient.post.and.returnValue(throwError(errorResponse));

    service.createLine('Line 1', 'BUS', null).subscribe(
      line => fail(),
      err => expect(err.message).toContain('400 Bad Request')
    );
  });

  it('get - success', () => {
    const expectedLine = new Line(1, 'Line 1', 'BUS', [], [], 'route');
    httpClient.get.and.returnValue(of(expectedLine));

    service.getLine(1).subscribe(
      line => {
        expect(line).toBe(expectedLine);
      },
      err => fail()
    );

    expect(httpClient.get.calls.count()).toBe(1);
  });

  it('get - not found', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Line not found',
      status: 404, statusText: 'Not Found'
    });

    httpClient.get.and.returnValue(throwError(errorResponse));

    service.getLine(99).subscribe(
      zone => fail(),
      err => expect(err.message).toContain('404 Not Found')
    );
  });

  it('get all - success', () => {
    const expectedLines = [
      new Line(1, 'Line 1', 'BUS', [], [], 'route'),
      new Line(2, 'Line 2', 'METRO', [], [], 'route'),
    ];
    httpClient.get.and.returnValue(of(expectedLines));

    service.getLines().subscribe(
      lines => {
        expect(lines).toBe(expectedLines);
      },
      err => fail()
    );

    expect(httpClient.get.calls.count()).toBe(1);
  });

  it('delete - success', () => {
    const expectedLine = new Line(1, 'Line 1', 'BUS', [], [], 'route');
    httpClient.delete.and.returnValue(of(expectedLine));

    service.deleteLine(1).subscribe(
      line => {
        expect(line).toBe(expectedLine);
      },
      err => fail()
    );

    expect(httpClient.delete.calls.count()).toBe(1);
  });

  it('delete - not found', () => {
    const errorResponse = new HttpErrorResponse({
      error: 'Zone not found',
      status: 404, statusText: 'Not Found'
    });

    httpClient.delete.and.returnValue(throwError(errorResponse));

    service.deleteLine(99).subscribe(
      line => fail(),
      err => expect(err.message).toContain('404 Not Found')
    );
  });

});
