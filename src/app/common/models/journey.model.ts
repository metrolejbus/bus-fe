import { Line } from './line.model';
import { TimedLocation } from './timed-location.model';

export class Journey {

  public constructor(
    public id: number = null,
    public line: Line = null,
    public departureTime: string = null,
    public arrivalTime: string = null,
    public locations: TimedLocation[] = null,
  ) {
    this.locations = this.locations || [];
  }
}
