export class Zone {

  public constructor(
    public id: number = null,
    public name: string = null
  ) {}
}
