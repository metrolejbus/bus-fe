import { Zone } from './zone.model';

export class Station {

  public constructor(
    public id: number = null,
    public name: string = null,
    public zone: Zone = null,
    public location: { lat: number, lng: number } = null,
  ) {
    this.location = this.location || { lat: null, lng: null };
  }
}
