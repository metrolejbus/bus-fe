import { Station } from './station.model';
import { LineService } from '../services/line.service';
import { Schedule } from 'src/app/models/schedule.model';

export class Line {

  public constructor(
    public id: number = null,
    public name: string = null,
    public type: string = null,
    public stations: Station[] = [],
    public schedules?: Schedule[],
    public route?: String
  ) {
  }
}
