export class TimedLocation {

  public constructor(
    public lineId: number = null,
    public journeyId: number = null,
    public lat: number = null,
    public lng: number = null,
    public timestamp: string = null,
  ) {}
}
