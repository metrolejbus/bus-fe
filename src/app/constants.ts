const MAP_ICONS = {
  'BUS': 'directions_bus',
  'METRO': 'directions_railway',
  'TRAM': 'tram'
};

const MAP_ICONS_COLOR = {
  'BUS': '#0277bd',
  'METRO': '#d84315',
  'TRAM': '#00695c'
};

export {
  MAP_ICONS,
  MAP_ICONS_COLOR
};
