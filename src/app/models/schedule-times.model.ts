import { Time } from '@angular/common';


export class ScheduleTimes {
  public constructor(
    public routeA: Time[] = [],
    public routeB: Time[] = []
  ) { }
}
