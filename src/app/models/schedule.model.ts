import { Line } from 'src/app/common/models/line.model';
import { DaysOfWeek } from './days-of-week.model';
import { Time } from '@angular/common';

export class Schedule {

  public constructor(
    public id: number = null,
    public routeA: string[] = [],
    public routeB: string[] = [],
    public line: number = null,
    public appliesTo: DaysOfWeek[] = [],
    public startDate: Date = new Date(),
    public endDate: Date = new Date()
  ) { }
}
