import { browser, ElementFinder, element, by } from 'protractor';
import { AppPage } from '../app.po';


export class AdminPage extends AppPage {

  protected tabs: {
    zones: ElementFinder,
    stations: ElementFinder,
    lines: ElementFinder,
  };

  constructor() {
    super();
    this.tabs = {
      zones: element(by.xpath('//a[.//strong="Zones"]')),
      stations: element(by.xpath('//a[.//strong="Stations"]')),
      lines: element(by.xpath('//a[.//strong="Lines"]')),
    };
  }

  navigateTo() {
    return browser.get('/admin');
  }

  changeTab(tab: string) {
    this.tabs[tab].click();
  }
}
