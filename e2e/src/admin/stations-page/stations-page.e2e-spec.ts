import { LoginPage } from '../../auth/login-page/login-page.po';
import { StationPage } from './stations-page.po';
import { browser } from 'protractor';


describe('stations-page testing', () => {
  let page: StationPage;

  beforeAll(() => {
    const loginPage = new LoginPage();
    loginPage.navigateTo();
    loginPage.login('admin', 'password');

    expect(browser.getCurrentUrl()).toBe('http://localhost:4201/home');
  });

  beforeEach(() => {
    page = new StationPage();
    page.navigateTo();
    page.changeTab('stations');

    expect(browser.getCurrentUrl()).toBe('http://localhost:4201/admin/stations');
  });

  it('should show zone error', async () => {
    page.fillName('New Station');
    page.fillZone('ABC');
    page.fillLat('51.5120');
    page.fillLng('-0.0941');
    page.saveForm();

    expect(await page.isZoneErrorShown()).toBeTruthy();
    expect(await page.isSaveDisabled()).toBeTruthy();
    expect(await page.isFormEmpty()).toBeFalsy();
  });

  it('should show lat error', async () => {
    page.fillName('New Station');
    page.fillZone('II');
    page.fillLat('99');
    page.fillLng('-0.0941');
    page.saveForm();
    browser.sleep(50000);

    expect(await page.isLatErrorShown()).toBeTruthy();
    expect(await page.isSaveDisabled()).toBeTruthy();
    expect(await page.isFormEmpty()).toBeFalsy();

    page.clearForm();
    browser.sleep(10000);

    page.fillName('New Station');
    page.fillZone('II');
    page.fillLat('-99');
    page.fillLng('-0.0941');
    page.saveForm();

    expect(await page.isLatErrorShown()).toBeTruthy();
    expect(await page.isSaveDisabled()).toBeTruthy();
    expect(await page.isFormEmpty()).toBeFalsy();
  });

  it('should show lng error', async () => {
    page.fillName('New Station');
    page.fillZone('II');
    page.fillLat('51.5120');
    page.fillLng('199');
    page.saveForm();
    browser.sleep(20000);

    expect(await page.isLngErrorShown()).toBeTruthy();
    expect(await page.isSaveDisabled()).toBeTruthy();
    expect(await page.isFormEmpty()).toBeFalsy();

    page.clearForm();

    page.fillName('New Station');
    page.fillZone('II');
    page.fillLat('51.5120');
    page.fillLng('-199');
    page.saveForm();

    expect(await page.isLngErrorShown()).toBeTruthy();
    expect(await page.isSaveDisabled()).toBeTruthy();
    expect(await page.isFormEmpty()).toBeFalsy();
  });

  it('should clear form', async () => {
    page.fillName('New Station');
    page.fillZone('II');
    page.fillLat('51.5120');
    page.fillLng('-0.0941');
    page.clearForm();

    expect(await page.isFormEmpty()).toBeTruthy();
  });

  it('should filter', async () => {
    expect(await page.getNumberOfRows()).toBe(12);

    page.filter('London');
    expect(await page.getNumberOfRows()).toBe(1);
    expect(await page.doAllRowsContain('X')).toBeTruthy();
  });

  it('should clear filter', async () => {
    expect(await page.getNumberOfRows()).toBe(12);

    page.filter('London');
    expect(await page.getNumberOfRows()).toBe(1);

    await page.clearFilter();
    expect(await page.getNumberOfRows()).toBe(12);
  });

  it('should select station', async () => {
    await page.selectRow(8);

    expect(await page.isFormEmpty()).toBeFalsy();
    expect(await page.getNameValue()).toBe('Union Street Station');
  });

  it('should update station', async () => {
    const numRows = await page.getNumberOfRows();

    await page.selectRow(8);

    await page.clearName();
    page.fillName('New Station');
    page.saveForm();

    expect(await page.isStationPresent('New Station')).toBeTruthy();
    expect(await page.isStationPresent('Union Street Station')).toBeFalsy();
    expect(await page.getNumberOfRows()).toBe(numRows);
  });

  it('should hide clear checked btn', async() => {
    expect(await page.isClearCheckedVisible()).toBeFalsy();
  });

  it('should check stations', async () => {
    await page.checkRows([3, 6]);

    expect(await page.isRowChecked(3)).toBeTruthy();
    expect(await page.isRowChecked(6)).toBeTruthy();
    expect(await page.isClearCheckedVisible()).toBeTruthy();
  });

  it('should uncheck stations', async () => {
    await page.checkRows([3, 6]);
    await page.uncheckRows([3]);

    expect(await page.isRowChecked(3)).toBeFalsy();
    expect(await page.isRowChecked(6)).toBeTruthy();
    expect(await page.isClearCheckedVisible()).toBeTruthy();

    await page.uncheckRows([6]);

    expect(await page.isRowChecked(6)).toBeFalsy();
    expect(await page.isClearCheckedVisible()).toBeFalsy();
  });

  it('should clear checked stations', async () => {
    await page.checkRows([3, 6]);
    await page.clearChecked();

    expect(await page.isRowChecked(3)).toBeFalsy();
    expect(await page.isRowChecked(6)).toBeFalsy();
    expect(await page.isClearCheckedVisible()).toBeFalsy();
  });

  it('should disable delete', async () => {
    expect(await page.isDeleteDisabled()).toBeTruthy();
  });

  it('should enable delete', async () => {
    await page.checkRows([3, 6]);
    expect(await page.isDeleteDisabled()).toBeFalsy();
  });

  it('should delete checked rows', async () => {
    await page.checkRows([1, 2]);
    await page.deleteChecked();

    expect(await page.isDeleteDisabled()).toBeTruthy();
    expect(await page.isClearCheckedVisible()).toBeFalsy();
    expect(await page.isStationPresent('Mansion House Station')).toBeFalsy();
    expect(await page.isStationPresent('Millennium Bridge Station')).toBeFalsy();
  });

  afterAll(() => {
    page.navBar.navigate('logout');
  });
});
