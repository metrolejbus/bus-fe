import {
  browser,
  ElementFinder,
  element,
  by,
  ElementArrayFinder,
  ExpectedConditions,
} from 'protractor';
import { AdminPage } from '../admin-page.po';

export class StationPage extends AdminPage {

  private form: {
    nameField: ElementFinder,
    zoneField: ElementFinder,
    latField: ElementFinder,
    lngField: ElementFinder,
    saveBtn: ElementFinder,
    clearBtn: ElementFinder,
  };

  private filterField: ElementFinder;
  private deleteBtn: ElementFinder;

  constructor() {
    super();
    this.form = {
      nameField: element(by.id('name-field')),
      zoneField: element(by.id('zone-field')),
      latField: element(by.id('lat-field')),
      lngField: element(by.id('lng-field')),
      saveBtn: element(by.id('save-btn')),
      clearBtn: element(by.id('clear-btn')),
    };

    this.filterField = element(by.id('filter-field'));
    this.deleteBtn = element(by.id('delete-btn'));
  }

  navigateTo() {
    return browser.get('/admin/stations');
  }

  public saveForm(): void {
    browser.wait(ExpectedConditions.presenceOf(this.form.saveBtn), 5000);
    this.form.saveBtn.click();
  }

  public clearForm(): void {
    browser.wait(ExpectedConditions.presenceOf(this.form.clearBtn), 5000);
    this.form.clearBtn.click();
  }

  public fillName(name: string): void {
    browser.wait(ExpectedConditions.presenceOf(this.form.nameField), 5000);
    this.form.nameField.sendKeys(name);
  }

  public async getNameValue(): Promise<string> {
    return this.form.nameField.getAttribute('value');
  }

  public async clearName(): Promise<any> {
    return this.form.nameField.clear();
  }

  public async fillZone(name: string): Promise<void> {
    browser.wait(ExpectedConditions.presenceOf(this.form.zoneField), 5000);
    this.form.zoneField.click();

    browser.wait(ExpectedConditions.presenceOf(
      element(by.xpath('//div[contains(@class, "mat-autocomplete-panel ")]'))
    ), 5000);

    const el = await element.all(by.xpath(`//*[contains(@class, "mat-option ") and .//span="I"]`));

    browser.sleep(15000);
    if (el.length === 1) {
      el[0].click();
    } else {
      this.form.zoneField.sendKeys(name);
    }
  }

  public async getZoneValue(): Promise<string> {
    return this.form.zoneField.getAttribute('value');
  }

  public async clearZone(): Promise<any> {
    return this.form.zoneField.clear();
  }

  public fillLat(lat: string): void {
    browser.wait(ExpectedConditions.presenceOf(this.form.latField), 5000);
    this.form.latField.sendKeys(lat);
  }

  public async getLatValue(): Promise<string> {
    return this.form.latField.getAttribute('value');
  }

  public async clearLat(): Promise<any> {
    return this.form.latField.clear();
  }

  public fillLng(lng: string): void {
    browser.wait(ExpectedConditions.presenceOf(this.form.lngField), 5000);
    this.form.lngField.sendKeys(lng);
  }

  public async getLngValue(): Promise<string> {
    return this.form.lngField.getAttribute('value');
  }

  public async clearLng(): Promise<any> {
    return this.form.lngField.clear();
  }

  public async selectRow(num: number): Promise<void> {
    return element(by.xpath(`//table[@id="table"]//tr[${num}]//td[3]`)).click();
  }

  public filter(term: string): void {
    this.filterField.sendKeys(term);
  }

  public async clearFilter(): Promise<any> {
    await this.filterField.clear();
    return this.filterField.sendKeys(' ');
  }

  public async checkRows(nums: number[]): Promise<void> {
    for (const num of nums) {
      await element(by.xpath(`//table//tr[${num}]//*[contains(@class, "mat-checkbox ")]//label`)).click();
    }
  }

  public async uncheckRows(nums: number[]): Promise<void> {
    return this.checkRows(nums);
  }

  public async deleteChecked(): Promise<void> {
    return this.deleteBtn.click();
  }

  public async clearChecked(): Promise<void> {
    return element(by.id('table-selection-clear-btn')).click();
  }

  public async getNumberOfRows(): Promise<number> {
    return element.all(by.xpath('//table[@id="table"]//tr[contains(@class, "mat-row")]')).count();
  }

  public async isFormEmpty(): Promise<boolean> {
    const name = await this.form.nameField.getAttribute('value');
    return name.length === 0;
  }

  public async isStationPresent(name: string): Promise<boolean> {
    const el = element(by.xpath(`//table[@id="table"]//tr[contains(@class, "mat-row")]//td[3 and .="${name}"]`));
    return el.isPresent();
  }

  public async isZoneErrorShown(): Promise<boolean> {
    return element(by.id('zone-error')).isPresent();
  }

  public async isLatErrorShown(): Promise<boolean> {
    const min = await element(by.id('lat-error-min'));
    const max = await element(by.id('lat-error-max'));
    return min.isPresent() || max.isPresent();
  }

  public async isLngErrorShown(): Promise<boolean> {
    const min = await element(by.id('lng-error-min'));
    const max = await element(by.id('lng-error-max'));
    return min.isPresent() || max.isPresent();
  }

  public async isSaveDisabled(): Promise<boolean> {
    return await this.form.saveBtn.getAttribute('disabled') === 'true';
  }

  public async isDeleteDisabled(): Promise<boolean> {
    return await this.deleteBtn.getAttribute('disabled') === 'true';
  }

  public async isClearCheckedVisible(): Promise<boolean> {
    const el = await element.all(by.xpath('//*[@id="table-selection-clear-btn"]'));
    return el.length === 1;
  }

  public async isRowChecked(num: number): Promise<boolean> {
    const el = await element.all(by.xpath(`//table//tr[${num}]//*[contains(@class, "mat-checkbox-checked")]`));
    return el.length === 1;
  }

  public async doAllRowsContain(term: string): Promise<boolean> {
    // @ts-ignore
    const names: ElementArrayFinder = await element.all(by.xpath('//table[@id="table"]//tr[contains(@class, "mat-row")]//td[3]'));
    return !! names.filter(async name => await name.getText() === term).length;
  }
}
