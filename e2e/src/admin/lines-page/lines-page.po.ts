import {
  browser,
  ElementFinder,
  element,
  by,
  ElementArrayFinder,
  ExpectedConditions,
} from 'protractor';
import { AdminPage } from '../admin-page.po';

export class LinePage extends AdminPage {

  private form: {
    nameField: ElementFinder,
    typeField: ElementFinder,
    saveBtn: ElementFinder,
    clearBtn: ElementFinder,
    stationsBtn: ElementFinder;
  };

  private filterField: ElementFinder;
  private deleteBtn: ElementFinder;

  constructor() {
    super();
    this.form = {
      nameField: element(by.id('name-field')),
      typeField: element(by.id('type-field')),
      saveBtn: element(by.id('save-btn')),
      clearBtn: element(by.id('clear-btn')),
      stationsBtn: element(by.id('stations-dialog-btn')),
    };

    this.filterField = element(by.id('filter-field'));
    this.deleteBtn = element(by.id('delete-btn'));
  }

  navigateTo() {
    return browser.get('/admin/lines');
  }

  public saveForm(): void {
    browser.wait(ExpectedConditions.presenceOf(this.form.saveBtn), 5000);
    this.form.saveBtn.click();
  }

  public clearForm(): void {
    browser.wait(ExpectedConditions.presenceOf(this.form.clearBtn), 5000);
    this.form.clearBtn.click();
  }

  public fillName(name: string): void {
    browser.wait(ExpectedConditions.presenceOf(this.form.nameField), 5000);
    this.form.nameField.sendKeys(name);
  }

  public async getNameValue(): Promise<string> {
    return this.form.nameField.getAttribute('value');
  }

  public async clearName(): Promise<any> {
    return this.form.nameField.clear();
  }

  public async fillType(name: string): Promise<void> {
    browser.wait(ExpectedConditions.presenceOf(this.form.typeField), 5000);
    this.form.typeField.sendKeys(name);
  }

  public async geTypeValue(): Promise<string> {
    return this.form.typeField.getAttribute('value');
  }

  public async clearType(): Promise<any> {
    return this.form.typeField.clear();
  }

  public async selectRow(num: number): Promise<void> {
    return element(by.xpath(`//table[@id="table"]//tr[${num}]//td[3]`)).click();
  }

  public filter(term: string): void {
    this.filterField.sendKeys(term);
  }

  public async clearFilter(): Promise<any> {
    await this.filterField.clear();
    return this.filterField.sendKeys(' ');
  }

  public async checkRows(nums: number[]): Promise<void> {
    for (const num of nums) {
      await element(by.xpath(`//table//tr[${num}]//*[contains(@class, "mat-checkbox ")]//label`)).click();
    }
  }

  public async uncheckRows(nums: number[]): Promise<void> {
    return this.checkRows(nums);
  }

  public async deleteChecked(): Promise<void> {
    return this.deleteBtn.click();
  }

  public async clearChecked(): Promise<void> {
    return element(by.id('table-selection-clear-btn')).click();
  }

  public async getNumberOfRows(): Promise<number> {
    return element.all(by.xpath('//table[@id="table"]//tr[contains(@class, "mat-row")]')).count();
  }

  public async isFormEmpty(): Promise<boolean> {
    const name = await this.form.nameField.getAttribute('value');
    return name.length === 0;
  }

  public async isLinePresent(name: string): Promise<boolean> {
    const el = element(by.xpath(`//table[@id="table"]//tr[contains(@class, "mat-row")]//td[3 and .="${name}"]`));
    return el.isPresent();
  }

  public async isSaveDisabled(): Promise<boolean> {
    return await this.form.saveBtn.getAttribute('disabled') === 'true';
  }

  public async isDeleteDisabled(): Promise<boolean> {
    return await this.deleteBtn.getAttribute('disabled') === 'true';
  }

  public async isClearCheckedVisible(): Promise<boolean> {
    const el = await element.all(by.xpath('//*[@id="table-selection-clear-btn"]'));
    return el.length === 1;
  }

  public async isRowChecked(num: number): Promise<boolean> {
    const el = await element.all(by.xpath(`//table//tr[${num}]//*[contains(@class, "mat-checkbox-checked")]`));
    return el.length === 1;
  }

  public async doAllRowsContain(term: string): Promise<boolean> {
    // @ts-ignore
    const names: ElementArrayFinder = await element.all(by.xpath('//table[@id="table"]//tr[contains(@class, "mat-row")]//td[3]'));
    return !! names.filter(async name => await name.getText() === term).length;
  }
}
