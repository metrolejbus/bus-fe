import { LoginPage } from '../../auth/login-page/login-page.po';
import { browser } from 'protractor';
import { LinePage } from './lines-page.po';


describe('lines-page testing', () => {
  let page: LinePage;

  beforeAll(() => {
    const loginPage = new LoginPage();
    loginPage.navigateTo();
    loginPage.login('admin', 'password');

    expect(browser.getCurrentUrl()).toBe('http://localhost:4201/home');
  });

  beforeEach(() => {
    page = new LinePage();
    page.navigateTo();
    page.changeTab('lines');

    expect(browser.getCurrentUrl()).toBe('http://localhost:4201/admin/lines');
  });

  it('should clear form', async () => {
    page.fillName('New Station');
    page.fillType('BUS');
    page.clearForm();

    expect(await page.isFormEmpty()).toBeTruthy();
  });

  it('should filter', async () => {
    expect(await page.getNumberOfRows()).toBe(2);

    page.filter('101');
    expect(await page.getNumberOfRows()).toBe(1);
    expect(await page.doAllRowsContain('X')).toBeTruthy();
  });

  it('should clear filter', async () => {
    expect(await page.getNumberOfRows()).toBe(2);

    page.filter('102');
    expect(await page.getNumberOfRows()).toBe(1);

    await page.clearFilter();
    expect(await page.getNumberOfRows()).toBe(2);
  });

  it('should select line', async () => {
    await page.selectRow(2);

    expect(await page.isFormEmpty()).toBeFalsy();
    expect(await page.getNameValue()).toBe('102');
  });

  it('should update line', async () => {
    const numRows = await page.getNumberOfRows();

    await page.selectRow(2);

    await page.clearName();
    page.fillName('999');
    page.saveForm();

    expect(await page.isLinePresent('999')).toBeTruthy();
    expect(await page.isLinePresent('102')).toBeFalsy();
    expect(await page.getNumberOfRows()).toBe(numRows);
  });

  it('should hide clear checked btn', async() => {
    expect(await page.isClearCheckedVisible()).toBeFalsy();
  });

  it('should check lines', async () => {
    await page.checkRows([1]);
    expect(await page.isRowChecked(1)).toBeTruthy();
    expect(await page.isClearCheckedVisible()).toBeTruthy();
  });

  it('should uncheck lines', async () => {
    await page.checkRows([1, 2]);
    await page.uncheckRows([1]);

    expect(await page.isRowChecked(1)).toBeFalsy();
    expect(await page.isRowChecked(2)).toBeTruthy();
    expect(await page.isClearCheckedVisible()).toBeTruthy();

    await page.uncheckRows([2]);

    expect(await page.isRowChecked(2)).toBeFalsy();
    expect(await page.isClearCheckedVisible()).toBeFalsy();
  });

  it('should clear checked lines', async () => {
    await page.checkRows([1, 2]);
    await page.clearChecked();

    expect(await page.isRowChecked(1)).toBeFalsy();
    expect(await page.isRowChecked(2)).toBeFalsy();
    expect(await page.isClearCheckedVisible()).toBeFalsy();
  });

  it('should disable delete', async () => {
    expect(await page.isDeleteDisabled()).toBeTruthy();
  });

  it('should enable delete', async () => {
    await page.checkRows([1]);
    expect(await page.isDeleteDisabled()).toBeFalsy();
  });

  it('should delete checked rows', async () => {
    await page.checkRows([1]);
    await page.deleteChecked();

    expect(await page.isDeleteDisabled()).toBeTruthy();
    expect(await page.isClearCheckedVisible()).toBeFalsy();
    expect(await page.isLinePresent('101')).toBeFalsy();
  });

  afterAll(() => {
    page.navBar.navigate('logout');
  });
});
