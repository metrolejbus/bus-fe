import { LoginPage } from '../../auth/login-page/login-page.po';
import { ZonePage } from './zone-page.po';
import { browser } from 'protractor';


describe('zone-page testing', () => {
  let page: ZonePage;

  beforeAll(() => {
    const loginPage = new LoginPage();
    loginPage.navigateTo();
    loginPage.login('admin', 'password');

    expect(browser.getCurrentUrl()).toBe('http://localhost:4201/home');
  });

  beforeEach(() => {
    page = new ZonePage();
    page.navigateTo();

    expect(browser.getCurrentUrl()).toBe('http://localhost:4201/admin/zones');
  });

  it('should add zone', async () => {
    page.fillName('XI');
    page.saveForm();

    expect(await page.isZonePresent('XI')).toBeTruthy();
    expect(await page.isErrorShown()).toBeFalsy();
    expect(await page.isFormEmpty()).toBeTruthy();
  });

  it('should show error', async () => {
    page.fillName('I');
    page.saveForm();

    expect(await page.isErrorShown()).toBeTruthy();
    expect(await page.isFormEmpty()).toBeFalsy();

    browser.refresh();

    page.fillName('');
    page.saveForm();

    expect(await page.isErrorShown()).toBeTruthy();
  });

  it('should clear form', async () => {
    page.fillName('XII');
    page.clearForm();

    expect(await page.isFormEmpty()).toBeTruthy();
  });

  it('should disable save', async () => {
    expect(await page.isSaveDisabled()).toBeTruthy();

    page.fillName('');
    expect(await page.isSaveDisabled()).toBeTruthy();
  });

  it('should filter', async () => {
    expect(await page.getNumberOfRows()).toBe(19);

    page.filter('X');
    expect(await page.getNumberOfRows()).toBe(3);
    expect(await page.doAllRowsContain('X')).toBeTruthy();
  });

  it('should clear filter', async () => {
    expect(await page.getNumberOfRows()).toBe(19);

    page.filter('X');
    expect(await page.getNumberOfRows()).toBe(3);

    await page.clearFilter();
    expect(await page.getNumberOfRows()).toBe(19);
  });

  it('should select zone', async () => {
    await page.selectRow(6);

    expect(await page.isFormEmpty()).toBeFalsy();
    expect(await page.getNameValue()).toBe('III');
  });

  it('should update zone', async () => {
    const numRows = await page.getNumberOfRows();

    await page.selectRow(6);

    await page.clearName();
    page.fillName('XXIII');
    page.saveForm();

    expect(await page.isZonePresent('XXIII')).toBeTruthy();
    expect(await page.isZonePresent('III')).toBeFalsy();
    expect(await page.getNumberOfRows()).toBe(numRows);
  });

  it('should not update zone', async () => {
    await page.selectRow(6);

    await page.clearName();
    page.fillName('V');
    page.saveForm();

    expect(await page.isErrorShown()).toBeTruthy();
  });

  it('should hide clear checked btn', async() => {
    expect(await page.isClearCheckedVisible()).toBeFalsy();
  });

  it('should check zones', async () => {
    await page.checkRows([3, 6]);

    expect(await page.isRowChecked(3)).toBeTruthy();
    expect(await page.isRowChecked(6)).toBeTruthy();
    expect(await page.isClearCheckedVisible()).toBeTruthy();
  });

  it('should uncheck zones', async () => {
    await page.checkRows([3, 6]);
    await page.uncheckRows([3]);

    expect(await page.isRowChecked(3)).toBeFalsy();
    expect(await page.isRowChecked(6)).toBeTruthy();
    expect(await page.isClearCheckedVisible()).toBeTruthy();

    await page.uncheckRows([6]);

    expect(await page.isRowChecked(6)).toBeFalsy();
    expect(await page.isClearCheckedVisible()).toBeFalsy();
  });

  it('should clear checked zones', async () => {
    await page.checkRows([3, 6]);
    await page.clearChecked();

    expect(await page.isRowChecked(3)).toBeFalsy();
    expect(await page.isRowChecked(6)).toBeFalsy();
    expect(await page.isClearCheckedVisible()).toBeFalsy();
  });

  it('should disable delete', async () => {
    expect(await page.isDeleteDisabled()).toBeTruthy();
  });

  it('should enable delete', async () => {
    await page.checkRows([3, 6]);
    expect(await page.isDeleteDisabled()).toBeFalsy();
  });

  it('should delete checked rows', async () => {
    await page.checkRows([9, 10]);
    await page.deleteChecked();

    expect(await page.isDeleteDisabled()).toBeTruthy();
    expect(await page.isClearCheckedVisible()).toBeFalsy();
    expect(await page.isZonePresent('V')).toBeFalsy();
    expect(await page.isZonePresent('Va')).toBeFalsy();
  });

  afterAll(() => {
    page.navBar.navigate('logout');
  });
});
