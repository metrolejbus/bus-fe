import { browser } from 'protractor';
import { NavBar } from './common/navbar.po';

export class AppPage {
  public navBar: NavBar;

  constructor() {
    this.navBar = new NavBar();
  }

  navigateTo() {
    return browser.get('/');
  }
}
