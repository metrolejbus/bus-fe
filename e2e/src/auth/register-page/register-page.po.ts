import {browser, by, element, ElementFinder, protractor} from 'protractor';
import {AppPage} from '../../app.po';


export class RegisterPage extends AppPage {
  private emailField: ElementFinder;
  private userNameField: ElementFinder;
  private firstNameField: ElementFinder;
  private lastNameField: ElementFinder;
  private middleNameField: ElementFinder;
  private ageField: ElementFinder;
  private statusField: ElementFinder;
  private passwordField: ElementFinder;
  private submitBtn: ElementFinder;

  constructor() {
    super();

    this.emailField = element(by.id('email-field'));
    this.userNameField = element(by.id('username-field'));
    this.firstNameField = element(by.id('firstName-field'));
    this.lastNameField = element(by.id('lastName-field'));
    this.middleNameField = element(by.id('middleName-field'));
    this.ageField = element(by.id('age-field'));
    this.statusField = element(by.id('status-field'));
    this.passwordField = element(by.id('password-field'));
    this.submitBtn = element(by.id('submit-btn'));
  }

  navigateTo() {
    return browser.get('/auth/register');
  }

  register(
    email: string,
    userName: string,
    firstName: string,
    lastName: string,
    middleName: string,
    age: string,
    password: string,
    status: string
  ) {
    browser.wait(
      protractor.ExpectedConditions.presenceOf(this.userNameField),
      5000
    );

    this.emailField.sendKeys(email);
    this.userNameField.sendKeys(userName);
    this.firstNameField.sendKeys(firstName);
    this.lastNameField.sendKeys(lastName);
    this.middleNameField.sendKeys(middleName);
    this.ageField.sendKeys(age);
    this.statusField.sendKeys(status);
    this.passwordField.sendKeys(password);
    this.submitBtn.click();
  }
}
