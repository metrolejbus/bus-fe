import {RegisterPage} from './register-page.po';
import {browser} from 'protractor';

describe('login-testing', () => {
  let page: RegisterPage;

  beforeEach(() => {
    page = new RegisterPage();

    page.navigateTo();
    expect(browser.getCurrentUrl()).toBe('http://localhost:4200/auth/register');
  });

  it('should register', () => {
    page.register('t@t.c', 'username', 'firstName', 'lastName', 'middleName', '20', 'password', 'STUDENT');
    expect(browser.getCurrentUrl()).toBe('http://localhost:4200/home');
  });
});
