import {LoginPage} from './login-page.po';
import {browser} from 'protractor';

describe('login-testing', () => {
  let page: LoginPage;

  beforeEach(() => {
    page = new LoginPage();
    page.navigateTo();
    expect(browser.getCurrentUrl()).toBe('http://localhost:4200/auth/login');
  });

  it('should log in', () => {
    page.login('admin', 'password')
      .then(() => {
        browser.waitForAngular();
        browser.sleep(1000);
        expect(browser.getCurrentUrl()).toBe('http://localhost:4200/home');
      });
  });

  it('should get 403', () => {
    page.login('wrongUser', 'wrongPassword')
      .then(() => {
        browser.waitForAngular();
        browser.sleep(1000);
        expect(page.getValidationMessage().isDisplayed()).toBe(true);
        expect(page.getValidationMessage().getText())
          .toMatch('Wrong username/password combination');
      });
  });

  it('should display error when blank username', () => {
    page.login('', 'wrongPassword')
      .then(() => {
        browser.waitForAngular();
        browser.sleep(1000);
        expect(page.getUsernameError().isDisplayed()).toBe(true);
        expect(page.getUsernameError().getText())
          .toMatch('Username can not be empty');
      });
  });
});
