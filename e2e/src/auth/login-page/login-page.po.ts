import {browser, by, element, ElementFinder, protractor} from 'protractor';
import {AppPage} from '../../app.po';

export class LoginPage extends AppPage {

  private usernameField: ElementFinder;
  private usernameError: ElementFinder;
  private passwordField: ElementFinder;
  private passwordError: ElementFinder;
  private errorMessage: ElementFinder;
  private submitBtn: ElementFinder;

  constructor() {
    super();
    this.usernameField = element(by.id('username-field'));
    this.usernameError = element(by.id('username-error'));

    this.passwordField = element(by.id('password-field'));
    this.passwordError = element(by.id('password-error'));

    this.errorMessage = element(by.id('error-message'));

    this.submitBtn = element(by.id('submit-btn'));
  }

  navigateTo() {
    return browser.get('/auth/login');
  }

  login(username: string, password: string) {
    browser.wait(protractor.ExpectedConditions.presenceOf(this.usernameField), 5000);

    this.usernameField.sendKeys(username);
    this.passwordField.sendKeys(password);
    return this.submitBtn.click();
  }

  getValidationMessage() {
    return this.errorMessage;
  }

  getUsernameError() {
    return this.usernameError;
  }
}
