import { browser } from 'protractor';
import { AppPage } from '../app.po';

export class HomePage extends AppPage {

  navigateTo() {
    return browser.get('/home');
  }
}
