import { ElementFinder, element, by } from 'protractor';

export class NavBar {
  private links: {
    home: ElementFinder,
    admin: ElementFinder,
    login: ElementFinder,
    logout: ElementFinder,
    register: ElementFinder,
  };

  constructor() {
    this.links = {
      home: element(by.xpath('//button[.//strong="Home"]')),
      admin: element(by.xpath('//button[.//strong="Admin Panel"]')),
      login: element(by.xpath('//button[.//strong="Log In"]')),
      logout: element(by.xpath('//button[.//strong="Log Out"]')),
      register: element(by.xpath('//button[.//strong="Register"]')),
    };
  }

  navigate(link: string) {
    this.links[link].click();
  }
}
